'''
Created on 14 lug 2016

@author: Luca
'''
from thresholding_strategies import SigmoidStrategy, ProbabilityStrategy
from src.boltzmann import RBM
import numpy as np
from src.boltzmann.dataset_manager import stratified_partitioning

class Experiment(object):
    '''
    An Experiment object packs all the information needed to run an experiment on a RBM.
    Such information being:
    - dataset: list of binary values the net will learn
    - metrics: LearningMetrics object that keeps information about the learning run    
    - simulation_strategy: strategy used by the RBM during simulation phase
    - learning_strategy: strategy used by the RBM during learning phase
    '''
    def __init__(self, dataset, metrics, simulation_strategy, learning_strategy=ProbabilityStrategy(SigmoidStrategy())):
        self.dataset = dataset
        self.metrics = metrics
        self.learning_strategy = learning_strategy
        self.simulation_strategy = simulation_strategy
    
    
    def cross_validation(self, folds=3, partition_function=stratified_partitioning):
        '''
        Run the experiment
        
        Input
        - folds: number of folds in which the dataset will be divided
        - partition_function: function used to partition set into folds
        
        Returns the mean of the results of
        '''
        partition = partition_function(self.dataset, folds)
        results = []
        for i in range(folds):
            hidden = partition[i]
            visible = [item for sublist in partition[0:i]+partition[i+1:] for item in sublist]
            self.metrics.reset([len(hidden)])
            rbm = RBM(np.array([visible]), self.metrics, self.learning_strategy)
            #print rbm.metrics.iterations[0]
            results = results + [self.simulation_strategy.perform(rbm, visible, hidden)]
        return np.mean(results, axis=0)