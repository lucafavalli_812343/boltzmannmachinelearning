from math import exp
import numpy as np
import scipy.optimize as opt

def sigma(x):
    '''
    Calculate sigma function for a float value
    
    Input
    - x: float representing the exponent in the sigma function
    
    Returns a float
    '''
    return 1.0 / (1 + exp(-x)) 


def vec_sigma(x):
    '''
    Calculate sigma function over a numpy array of floats
    
    Input
    - x: the numpy array of floats to calculate the sigma function on
    
    Returns an array of floats or float lists (depending on the type of x)
    '''
    f = np.vectorize(lambda v: sigma(v))  
    return f(x)


def get_shift(alpha):
    '''
    Calculate the shift to be applied to sigma function to compensate dataset imbalance
    
    Input
    - alpha: float being the fraction #pos / #neg in the dataset
    
    Returns the shift to be applied to the function
    '''
    f = lambda s: np.log(1 + np.exp(s)) - alpha * (np.log(1 + np.exp(s)) - s) 
    return opt.newton(f, 0)

def create_matrix(num_rows, num_columns):
    '''
    Create new matrix filled with zeroes
    
    Input
    - num_rows: integer number of rows the new matrix will have
    - num_columns: integer number of columns the new matrix will have
    
    Returns an array of lists of the desired dimensions filled with zeroes (float)
    '''
    return np.array([[0.] * num_columns] * num_rows)