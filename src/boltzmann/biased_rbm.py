'''
Created on 14 lug 2016

@author: Luca
'''
from src.boltzmann import RBM
import numpy as np

class BiasedRBM(RBM):
    '''
    Restricted Boltzmann Machine that uses bias nodes.
    '''
    def __init__(self, training_set, metrics, strategy):
        training_set = np.array([list(e) + [1] for e in training_set])
        metrics.hidden_dimensions = [value+1 for value in metrics.hidden_dimensions]
        super(BiasedRBM, self).__init__(training_set, metrics, strategy) 
        
        
    def simulate_hidden(self, visible):
        '''
        Add bias node to visible, then call parent
        '''
        visible = visible + [1]
        return super(BiasedRBM, self).simulate_hidden(self, visible)[:-1:]
    
    
    def simulate_visible(self, hidden):
        '''
        Add bias node to hidden, then call parent
        '''
        hidden = hidden + [1]
        return super(BiasedRBM, self).simulate_visible(self, hidden)[:-1:]