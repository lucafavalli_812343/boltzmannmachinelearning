'''
Created on 14 lug 2016

@author: Luca
'''
import numpy as np

class SimulationStrategy(object):
    '''
    SimulationStrategy applies a thresholding strategy on the simulated layer than calculates score.
    Attributes:
    - thresholding_strategy: thresholding strategy to be applied to the RBM
    - criterion: function used to calculate score
    '''

    def __init__(self, thresholding_strategy, criterion):
        self.thresholding_strategy = thresholding_strategy
        self.criterion = criterion
        
    
    def perform(self, rbm, visible, hidden):
        '''
        Perform the Simulation Strategy.
        
        Input
        - rbm: RBM object target of the experiment
        - visible: layer of binary labels to simulate
        - hidden: target layer of the learning process
        
        Returns the score of the simulation
        '''
        rbm.change_strategy(self.thresholding_strategy)
        simulated = rbm.simulate_hidden(visible)
        return self.criterion(simulated, hidden)
    
    
    
class LoopingSimulationStrategy(SimulationStrategy):
    '''
    When result of the feature detectors driving is non-deterministic, you may want to simulate many times to evaluate average.
    Attribute:
    - loops: how many times to iterate learning process        
    '''
    
    def __init__(self, thresholding_strategy, criterion, loops):
        self.loops = loops
        super(LoopingSimulationStrategy, self).__init__(thresholding_strategy, criterion)
        
    
    def perform(self, rbm, visible, hidden):
        rbm.change_strategy(self.thresholding_strategy)
        simulated = [rbm.simulate_hidden(visible) for _ in range(self.loops)]
        return np.mean(map(lambda x: self.criterion(x, hidden), simulated))