#Files in package
from dataset_manager import *
from utilities import *
from learning_metrics import *

class RBM(object):
    '''
    Restricted Boltzmann Machine class holds the function used to learn a dataset and simulation.
    Class attributes are:
    - metrics: LearningMetrics object that keeps information about the learning run
    - strategy: thresholding strategy object used to drive feature detectors
    - weights: state of the connections strengths of each layer
    '''
    def __init__(self, training_set, metrics, strategy):
        '''
        Initialize RBM attributes
        
        Input
        - training_set: array of binary lists of the examples the net will learn from
        - metrics: LearningMetrics object that keeps information about the learning run
        - strategy: thresholding strategy object used to drive feature detectors
        '''
        self.metrics = metrics
        self.strategy = strategy
        self.learn(training_set)    
      

    def learn(self, training_set):
        '''
        Update weights for the net, repeat until threshold is exceeded;
        metrics object values of d_max and iterations are updated
        
        Input
        - training_set: array of binary lists of the examples the net will learn from
        '''
        hidden_dimensions = self.metrics.hidden_dimensions
        convergence_threshold = self.metrics.convergence_threshold
        learning_rate = self.metrics.learning_rate
        
        tot_examples = len(training_set)                #number of examples in set
        visible_dimension = len(training_set[0])        #visible layer dimension
    
        weights = [create_matrix(*([visible_dimension] + hidden_dimensions)[i:i+2]) for i in range(len(hidden_dimensions))]    #starting weights; starts at 0
    
        #BEGIN
        for layer in range(len(weights)):            
            self.metrics.next_layer()
            while self.metrics.get_d_max() > convergence_threshold:
                hidden = self.drive_feature_detectors(training_set, weights[layer])
                visible = self.drive_feature_detectors(hidden, weights[layer].transpose())
                d_weights = (
                    learning_rate / tot_examples * (                 #scale factor
                        np.dot(training_set.transpose(), hidden) -   #result of running from visible to hidden
                        np.dot(visible.transpose(), hidden)          #result of running from hidden to visible
                    )          
                )
                
                weights[layer] += d_weights                 #update weights
                self.metrics.set_d_max(np.amax(abs(d_weights)))    #update delta max
                self.metrics.loop()
            training_set = self.drive_feature_detectors(training_set, weights[layer])    #Update training set for next layer
        self.weights = weights


    def drive_feature_detectors(self, layer, weights):
        '''
        Use strategy to navigate the RBM from the layer "layer" to the opposite
        This should be called only by other functions in RBM
        
        Input
        - layer: array of binary lists representing the layer we start navigating the net from
        - weights: array of float lists representing the weights for the connection in our net; rows are the starting layer, columns for the destination
        
        Returns an array of lists with the results of the extractions
        '''
        return self.strategy.perform(layer, weights)


    def simulate_hidden(self, visible):
        '''
        Simulate the execution of the net from visible layer to hidden.
        Use strategy to do so.
        
        Input
        - visible: list containing the integer values for the visible layer of the net
        
        Returns a list of integer values representing the simulated hidden layer
        '''
        for layer in self.weights:
            visible = self.drive_feature_detectors([visible], layer)
        
        return visible[0].tolist()
    
    
    def simulate_visible(self, hidden):
        '''
        Simulate the execution of the net from hidden layer to visible.
        Use strategy to do so.
        
        Input
        - hidden: list containing the integer values for the hidden layer of the net
        
        Returns a list of integer values representing the simulated visible layer
        '''
        for layer in self.weights[::-1]:
            hidden = self.drive_feature_detectors([hidden], layer.transpose())
        
        return hidden[0].tolist()
    
    
    def change_strategy(self, strategy):
        '''
        Change strategy used to navigate the RBM.
        
        Input
        - strategy: new strategy to be applied
        '''
        self.strategy = strategy