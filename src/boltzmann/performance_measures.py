'''
Created on 27 giu 2016

@author: Luca
'''
import numpy as np
from sklearn import metrics

def success_rate(got, expected):
    '''
    Number of nodes which have the same value in both the dataset and the simulation
    Input
    
    - got: array of the simulated layer labels
    - expected: array of the actual labels from the dataset
    
    Returns fraction of nodes which got the correct value
    '''
    return sum(expected == got)/float(len(got))

def true_positives(got, expected):
    '''
    Number of nodes which are on in both the simulation and the dataset
    
    Input
    - got: array of the simulated layer labels
    - expected: array of the actual labels from the dataset
    
    Returns the number of true positive nodes
    '''
    return sum(np.logical_and(got,expected))

def true_negatives(got, expected):
    '''
    Number of nodes which are off in both the simulation and the dataset
    
    Input
    - got: array of the simulated layer labels
    - expected: array of the actual labels from the dataset
    
    Returns the number of true negative nodes
    '''
    return sum(np.logical_not(np.logical_or(got, expected)))

def false_positives(got, expected):
    '''
    Number of nodes which are on in the simulation but should be off
    
    Input
    - got: array of the simulated layer labels
    - expected: array of the actual labels from the dataset
    
    Returns the number of false positive nodes
    '''
    return sum(np.logical_and(got, np.logical_not(expected)))

def false_negatives(got, expected):
    '''
    Number of nodes which are off in the simulation but should be on
    
    Input
    - got: array of the simulated layer labels
    - expected: array of the actual labels from the dataset
    
    Returns the number of false negative nodes
    '''
    return sum(np.logical_and(np.logical_not(got), expected))

def precision(got, expected):
    '''
    Fraction of positive nodes of the simulation that are actually positive
    
    Input
    - got: array of the simulated layer labels
    - expected: array of the actual labels from the dataset
    
    Returns the precision of the simulation
    '''
    tp = true_positives(got, expected)
    fp = false_positives(got, expected)
    return tp / (tp + fp)

def recall(got, expected):
    '''
    Fraction of positive nodes 
    
    Input
    - got: array of the simulated layer labels
    - expected: array of the actual labels from the dataset
    
    Returns the recall of the simulation
    '''
    tp = true_positives(got, expected)
    fn = false_negatives(got, expected)
    return tp / (tp + fn)

def F_score(got, expected):
    '''
    Harmonic mean between precision and recall
    
    Input
    - got: array of the simulated layer labels
    - expected: array of the actual labels from the dataset
    
    Returns the F-score of the simulation    
    '''
    tp = true_positives(got, expected)
    fp = false_positives(got, expected)
    fn = false_negatives(got, expected)
    return (2.0 * tp) / (2.0 * tp + fp + fn)


def area_under_ROC_curve(got, expected):
    '''
    Calculate the area under the receiver operating characteristic curve
    
    Input
    - got: array of the simulated layer labels
    - expected: array of float values got from the simulation
    
    Returns the area under the receiver operating characteristic curve from predictions score
    '''
    return metrics.roc_auc_score(expected, got)


def area_under_PR_curve(got, expected):
    '''
    Calculate di area under Precision-Recall curve
    
    Input
    - got: array of the simulated layer labels
    - expected: array of float values got from the simulation
    
    Returns the area under the Precision-Recall curve from predictions score
    '''
    precision, recall, _ = metrics.precision_recall_curve(expected, got)
    return metrics.auc(recall, precision)