'''
Created on 14 lug 2016

@author: Luca
'''

class LearningMetrics(object):
    '''
    A LearningMetrics object is used to keep track of the important parameters playing a part in RBM learning.
    Such parameters are:
    - hidden_dimensions: integer list representing the number of nodes each of the hidden layers should have
    - learning_rate: float value. Every loop we multiply the result of navigating the net in both directions by this factor
    - convergence_threshold: float value. No more iterations when the Delta in weights is lower than this value
    - max_iterations: integer number of maximum iterations. StopIteration exception is raised if this value is surpassed
    - iterations: integer number of times the learning process iterated
    - d_max: at the end of the learning process, this will be set to the values of delta which caused the exit from the loop
    - layer: layer the RBM is learning now
    '''
    
    def __init__(self, hidden_dimensions, learning_rate=0.01, convergence_threshold=10e-4, max_iterations=1000):
        '''
        Constructor
        '''
        self.hidden_dimensions = hidden_dimensions
        self.learning_rate = learning_rate
        self.convergence_threshold = convergence_threshold
        self.max_iterations = max_iterations
        self.iterations = [0] * len(hidden_dimensions)
        self.d_max = [float('inf')] * len(hidden_dimensions)
        self.layer = -1
         
        
    def loop(self):
        '''
        Increment iterations counter.
        Raises StopIteration if max_iterations is exceeded
        '''
        self.iterations[self.layer] += 1
        if self.iterations[self.layer] > self.max_iterations:
            raise StopIteration('Reached maximum number of iterations: %d' % self.max_iterations)
    
    
    def set_d_max(self, d):
        '''
        Set the d_max for the current layer
        
        Input
        - d: the value d_max will be set to
        '''
        self.d_max[self.layer] = d
    
    
    def get_d_max(self):
        '''
        Return d_max for the current layer
        '''
        return self.d_max[self.layer]
    
    
    def next_layer(self):
        '''
        The RBM is learning a new layer; increment layer counter
        '''
        self.layer += 1
        
        
    def reset(self, hidden_dimensions):
        '''
        Resets the learning metrics for a new learning cycle.
        Of course we will need to reset the hidden layer too.
        
        Input
        - hidden_dimensions: integer list representing the number of nodes each of the hidden layers should have
        '''
        self.hidden_dimensions = hidden_dimensions
        self.iterations = [0] * len(self.hidden_dimensions)
        self.d_max = [float('inf')] * len(self.hidden_dimensions)
        self.layer = -1