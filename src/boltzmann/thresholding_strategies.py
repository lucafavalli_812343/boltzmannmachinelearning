'''
Created on 14 lug 2016

@author: Luca
'''
from utilities import vec_sigma, get_shift
import numpy as np
from dataset_manager import positives, negatives

class IdentityStrategy(object):
    '''
    Thresholding strategy that simply returns the simulated layer as float values instead of applying threshold
    '''
    def __init__(self):
        pass
       
    def perform(self, layer, weights):
        '''
        Perform IdentityStrategy
        
        Input
        - layer: array of binary lists representing the layer we start navigating the net from
        - weights: array of float lists representing the weights for the connection in our net; rows are the starting layer, columns for the destination
        
        Returns the weighted sum navigation of the RBM
        '''
        return np.dot(layer, weights)
    


class FloatThresholdStratgy(object):
    '''
    Thresholding strategy that applies threshold to given matrix
    '''
    
    def __init__(self, threshold):
        self.threshold = threshold
        
    def perform(self, layer, weights):
        '''
        Perform FloatThresholdStrategy
        
        Input
        - layer: array of binary lists representing the layer we start navigating the net from
        - weights: array of float lists representing the weights for the connection in our net; rows are the starting layer, columns for the destination
        
        Returns the binary matrix result of threshold application  
        '''
        x = np.dot(layer, weights)
        return (x > self.threshold).astype(int)
    
    
    
class SigmoidStrategy(object):
    '''
    Apply sigmoid function to simulation results    
    '''
    
    def __init__(self):
        pass
    
    def perform(self, layer, weights):
        '''
        Perform SigmoidStrategy
        
        Input
        - layer: array of binary lists representing the layer we start navigating the net from
        - weights: array of float lists representing the weights for the connection in our net; rows are the starting layer, columns for the destination
        
        Returns the binary matrix result of random evaluation with sigmoid function  
        '''
        x = np.dot(layer, weights)
        return vec_sigma(x)
    


class ShiftedSigmoidStrategy(object):
    '''
    Apply sigmoid function to simulation results    
    A shift will be applied to balance positives and negatives in the set.   
    '''
    
    def __init__(self):
        pass
    
    def perform(self, layer, weights):
        '''
        Perform ShiftedSigmaThresholdStrategy
        
        Input
        - layer: array of binary lists representing the layer we start navigating the net from
        - weights: array of float lists representing the weights for the connection in our net; rows are the starting layer, columns for the destination
        
        Returns the binary matrix result of random evaluation with shifted sigmoid function  
        '''
        x = np.dot(layer, weights)
        alphas = np.array([float(positives(row)) / negatives(row) for row in layer])
        s = np.array([map(get_shift, alphas)] * len(x[0])).transpose()
        return vec_sigma(x - s)
    
    
class ProbabilityStrategy(object):
    '''
    Randomly generate float values to threshold the values obtained by the application of the sigmoid function over simulated layer.
    '''
    def __init__(self, strategy):
        self.strategy = strategy
    
    def perform(self, layer, weights):
        '''
        Perform ProbabilityStrategy
        
        Input
        - layer: array of binary lists representing the layer we start navigating the net from
        - weights: array of float lists representing the weights for the connection in our net; rows are the starting layer, columns for the destination
        
        Returns the binary matrix result of random evaluation on the given matrix
        '''
        x = self.strategy.perform(layer, weights)
        r = np.random.uniform(0, 1, (len(x), len(x[0])))
        return (r <= x).astype(int)