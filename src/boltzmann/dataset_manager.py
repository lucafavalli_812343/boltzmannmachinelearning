'''
Created on 14 mag 2016

@author: Luca
'''
import numpy as np
from random import sample


def load_data(filename, separator=' '):
    '''
    Read file with data set, one object per line expressed as space separated integer values
    
    Input
    - filename: string containing the pathname of the file to be read
    - separator: character dividing the columns of the input dataset; default being whitespace
    
    Returns an array of lists
    '''
    with open(filename) as f:
        input_values = [line.strip("\n\r").split(separator) for line in f.readlines()]
    return np.array(input_values).astype(int)


def positives(layer):
    '''
    Calculate the number of positives in the given layer
    
    Input
    - layer: set formed by 1s and 0s
    
    Returns the number of 1s in the set
    '''
    return np.sum(layer)


def negatives(layer):
    '''
    Calculate the number of negatives in the given layer
    
    Input
    - layer: set formed by 1s and 0s
    
    Returns the number of 0s in the set
    '''
    return len(layer) - positives(layer)


def random_partition(dataset, subsets):
    '''
    Create a random partition for a given dataset
    
    Input
    - dataset: dataset to create the partition on
    - subsets: number of subsets to divide the set in
    
    Returns a list of lists with the computed subsets
    '''
    dataset = np.random.permutation(dataset)
    n = len(dataset)
    size = n / subsets
    return [dataset[i*(size+1):(i+1)*(size+1)] if i<n%subsets else dataset[i*(size):(i+1)*(size)] for i in range(subsets)]


def split_and_randomize_set(dataset, min_positives=3, limit=10, how_many=50):
    '''
    Divide dataset in two subsets, one formed by columns of the dataset having equal or less than "limit" positives,
    the other by columns containing more than "limit" positives.
    Only columns with at least "min_positives" positives should be considered (default is 3 to guarantee 3-folds stratified sampling).
    Then return "how_many" random columns from each of the subsets"
    
    Input
    - dataset: matrix with values to be partitioned
    - min_positives: minimum number of positives we want in our set; it should always be lower than limit
    - limit: dividing element between the two subsets
    - how_many: how many columns we want from each subset
    
    Returns a tuple with the two extracted sets and their column indexes
    '''
    assert(min_positives < limit), "min_positives should always be lower than limit!"
    permutation, indexes = permute_columns(dataset)
    positives = np.ndarray.sum(permutation, axis=0)
    
    less_indexes = indexes[np.logical_and(positives >= min_positives, positives <= limit)]
    less_class = permutation[:, np.logical_and(positives >= min_positives, positives <= limit)]
    more_indexes = indexes[positives > limit]
    more_class = permutation[:, positives > limit]

    return (less_class[:,:how_many], less_indexes[:how_many], 
            more_class[:,:how_many], more_indexes[:how_many])

 
def permute_columns(mat):
    '''
    np.random.permutation shuffles matrix rows by default.
    We need to permute columns instead.
    
    Input:
    - mat: matrix to be shuffled
    
    Returns the shuffled matrix and their columns
    '''
    perm = sample(range(mat.shape[1]), mat.shape[1])
    return mat[:, perm], np.array(perm)


def stratified_partitioning(dataset, subsets=3):
    '''
    Partition dataset so that each subset have the same number of positives (or at least one more/less than the others)
    
    Input
    - dataset: dataset to create the partition on
    - subsets: number of subsets to divide the set in
    
    Returns a list of lists with the computed subsets
    '''
    positives = sum(dataset)
    negatives = len(dataset) - positives
    return ([np.random.permutation(l) for l in
            [z+([1]*(positives/subsets + 1)) if index>(subsets-1-(positives%3)) else z+[1]*(positives/subsets) for (index, z) in
            enumerate([[0]*(negatives/subsets + 1) if i<(negatives%subsets) else [0]*(negatives/subsets) for i in
            range(subsets)])]])