from src.boltzmann import load_data
from src.boltzmann.performance_measures import cross_validation
import numpy as np

loops = 100    #input how many times to simulate hidden
dataset = load_data('../options/allgenes-dataset/yeast.04_07_15_annotation.MF.allgenes.txt', '\t')

for term in range(10):
    for folds in [3, 5, 10]:
        print "LEARNING GO_TERM %d, WITH %d FOLDS" % (term, folds)
        print "\t\t%s" % str([cross_validation(dataset[:, term], folds, loops, t) for t in np.arange(0.1, 0.9, 0.1)])
