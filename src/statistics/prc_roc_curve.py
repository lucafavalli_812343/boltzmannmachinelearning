
# coding: utf-8

# In[1]:

from src.boltzmann import load_data, split_and_randomize_set
from src.boltzmann.performance_measures import area_under_ROC_curve, area_under_PR_curve
from src.boltzmann.thresholding_strategies import ShiftedSigmoidStrategy, SigmoidStrategy, ProbabilityStrategy
from src.boltzmann.learning_metrics import LearningMetrics
from src.boltzmann.experiment import Experiment
from src.boltzmann.simulation_strategies import SimulationStrategy


# In[2]:

def pr_and_roc(got, expected):
    return [area_under_PR_curve(got, expected), area_under_ROC_curve(got, expected)]

# In[3]:
experiment_strategy = SimulationStrategy(SigmoidStrategy(), pr_and_roc)
convergence_threshold = 10e-3
metrics = LearningMetrics([], convergence_threshold=convergence_threshold)

# In[4]:

dataset = load_data('../options/allgenes-dataset/yeast.04_07_15_annotation.MF.allgenes.txt', '\t')


# In[5]:

lower, class_lower, higher, class_higher = split_and_randomize_set(dataset)


# In[6]:

output_file_less="../../results/test180716_ROC_PR_less.csv"
output_file_more="../../results/test180716_ROC_PR_more.csv"
columns=["Class", "AUPRC", "AUROC"]


# In[7]:

with open(output_file_less, "w+") as out:
    out.write(",".join(columns))
    for i in range(len(lower[0, :])):
        cl = lower[:, i]
        experiment = Experiment(cl, metrics=metrics, simulation_strategy=experiment_strategy)
        [result_PRC, result_ROC] = experiment.cross_validation()
        out.write("\n%d,%f,%f" % (class_lower[i], result_PRC, result_ROC))


# In[8]:

with open(output_file_more, "w+") as out:
    out.write(",".join(columns))
    for i in range(len(higher[0, :])):
        cl = higher[:, i]
        experiment = Experiment(cl, metrics=metrics, simulation_strategy=experiment_strategy)
        [result_PRC, result_ROC] = experiment.cross_validation()
        out.write("\n%d,%f,%f" % (class_higher[i], result_PRC, result_ROC))


# In[9]:

output_file_less="../../results/test180716_SHIFTED_ROC_PR_less.csv"
output_file_more="../../results/test180716_SHIFTED_ROC_PR_more.csv"
experiment_strategy = SimulationStrategy(ShiftedSigmoidStrategy(), pr_and_roc)

# In[10]:

with open(output_file_less, "w+") as out:
    out.write(",".join(columns))
    for i in range(len(lower[0, :])):
        cl = lower[:, i]
        experiment = Experiment(cl, metrics=metrics, learning_strategy=ProbabilityStrategy(ShiftedSigmoidStrategy()), simulation_strategy=experiment_strategy)
        [result_PRC, result_ROC] = experiment.cross_validation()
        out.write("\n%d,%f,%f" % (class_lower[i], result_PRC, result_ROC))


# In[11]:

with open(output_file_more, "w+") as out:
    out.write(",".join(columns))
    for i in range(len(higher[0, :])):
        cl = higher[:, i]
        experiment = Experiment(cl, metrics=metrics, learning_strategy=ProbabilityStrategy(ShiftedSigmoidStrategy()), simulation_strategy=experiment_strategy)
        [result_PRC, result_ROC] = experiment.cross_validation()
        out.write("\n%d,%f,%f" % (class_higher[i], result_PRC, result_ROC))
