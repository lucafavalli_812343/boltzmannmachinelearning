import numpy as np
from src.boltzmann import load_data, stratified_partitioning, split_and_randomize_set
from src.boltzmann.performance_measures import area_under_PR_curve
from src.boltzmann.thresholding_strategies import SigmoidStrategy, ProbabilityStrategy
from src.boltzmann.learning_metrics import LearningMetrics
from src.boltzmann.experiment import Experiment
from src.boltzmann.simulation_strategies import SimulationStrategy

dataset = load_data('../options/allgenes-dataset/yeast.04_07_15_annotation.MF.allgenes.txt', '\t')

_, less_indexes, _, more_indexes = split_and_randomize_set(dataset=dataset, min_positives=3)
less_indexes.sort()
more_indexes.sort()

def run_experiment(output_file, indexes, metrics, learning_strategy, simulation_strategy, loops=25):
    columns = ['Molecular Function'] + map(str, np.arange(0.1, 1, 0.1))
    with open(output_file, 'w+') as out:
        out.write(','.join(columns))
        for i in range(len(indexes)):
            values = []
            for _ in range (loops):
                exp = Experiment(dataset[:, indexes[i]], metrics=metrics,simulation_strategy=simulation_strategy, learning_strategy=learning_strategy)
                values.append([exp.cross_validation(partition_function=stratified_partitioning)])
            out.write('\n%d,%s' % (indexes[i], ','.join(map(lambda x: '{0:.10f}'.format(x), np.mean(values, axis=0)))))

metrics = LearningMetrics([], learning_rate=10e-2, convergence_threshold=10e-3)

'''FIRST TEST'''
run_experiment('../../results/success_rate_less.csv', less_indexes, metrics,
               ProbabilityStrategy(SigmoidStrategy()), SimulationStrategy(SigmoidStrategy(), area_under_PR_curve))

'''SECOND TEST'''
run_experiment('../../results/success_rate_more.csv', more_indexes, metrics,
               ProbabilityStrategy(SigmoidStrategy()), SimulationStrategy(SigmoidStrategy(), area_under_PR_curve))