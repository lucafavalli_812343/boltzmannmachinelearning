from src.boltzmann import load_data, split_and_randomize_set
from src.boltzmann.performance_measures import cross_validation, F_score
import numpy as np

dataset = load_data('../options/allgenes-dataset/yeast.04_07_15_annotation.MF.allgenes.txt', '\t')

folds = 3
loops = 25    #input how many times to simulate hidden
debug_file="../../results/test060716_debug_number_of_positives_2.txt"
output_file="./../results/test060716_stratified_partitioning_2.txt"


lower, _, higher, _ = split_and_randomize_set(dataset)


# In[10]:

with open(output_file, "w+") as out:
    out.write("Less than 10 positives:\n")
    for i in range(len(lower[0,:])):
        result=[cross_validation(lower[:,i], folds, loops, t,
                               convergence_threshold=10e-3, criteria=F_score,
                               debug=True, debug_file=debug_file) 
                               for t in np.arange(0.1, 1.0, 0.1)]
        out.write("%s\n" % str(result))
    out.write("More than 10 positives:\n")
    for i in range(len(higher[0,:])):
        result=[cross_validation(higher[:,i], folds, loops, t,
                               convergence_threshold=10e-3, criteria=F_score,
                               debug=True, debug_file=debug_file) 
                               for t in np.arange(0.1, 1.0, 0.1)]
        out.write("%s\n" % str(result))