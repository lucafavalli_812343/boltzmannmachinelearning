import numpy as np

def max_with_indexes(lists):
    thresholds = np.arange(0.1, 1, 0.1)
    folds = [3, 5, 10]
    keys = ['Folds', 'Threshold', 'Value']
    
    results = [[round(thresholds[np.argmax(l)],1), max(l)] for l in lists]
    return dict(zip(keys, [folds[results.index(max(results, key = lambda x:x[1]))]] + (max(results, key = lambda x: x[1]))))

f = open('../results/test160516_all_genes.txt', 'r').read()
lines = f.split('\n')


#[max(map(lambda l: max(map(float, l[3:-1].split(','))), lines[6*i:6*(i+1)][1::2])) for i in range(10)]
print [max_with_indexes(map(lambda l: map(float, l[3:-1].split(',')), lines[6*i:6*(i+1)][1::2])) for i in range(10)]


f2 = open('../results/test300616_all_genes.txt').read()
lines2 = f2.split('\n')

print [max_with_indexes(map(lambda l: map(float, l[3:-1].split(',')), lines2[6*i:6*(i+1)][1::2])) for i in range(2)]