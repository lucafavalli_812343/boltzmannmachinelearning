il file yeast.04_07_15_annotation.MF.allgenes.txt e' separato da tabulazioni
e contiene una matrice  6224 x 1577, le cui righe sono i geni del lievito, le cui colonne
sono i termini Gene Ontology, Molecular Function (MF) con almeno una 
annotazione (valore 1 nella matrice) per i geni considerati.

I nomi dei geni sono nel file yeast.genes.all.txt
I nomi dei termini GO sono nel file GO.terms.MF.txt (per info vai su www.geneontology.org)

La rete con le connessioni tra gene (nodi del grafo) e' contenuta nel file 
yeast.network.wsum.unipred.MF.allgenes.txt, sempre separati da tab.


