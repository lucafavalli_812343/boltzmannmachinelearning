'''
Created on 30 giu 2016

@author: Luca
'''
import unittest
import src.boltzmann.performance_measures as pm
import numpy as np


class PositivesNegativesTest(unittest.TestCase):

    def testPositivesAndNegativesLength(self):
        '''Be sure true/false positives/negatives catch all cases'''
        for i in range(1000):
            e = np.random.randint(2, size=i)
            g = np.random.randint(2, size=i)
            self.assertEqual(len(e), pm.true_positives(g, e) + pm.true_negatives(g, e) + pm.false_positives(g, e) + pm.false_negatives(g, e), "Error in evaluating true/false positives/negatives: some cases are not covered")
            
    def testTruePositives(self):
        '''Be sure shortcut used to count true positives is correct'''
        for i in range(1000):
            e = np.random.randint(2, size=i)
            g = np.random.randint(2, size=i)
            counter = 0
            for j in range(i):
                if(e[j] == 1 and g[j] == 1):
                    counter += 1
            self.assertEqual(counter, pm.true_positives(g, e), "Error in evaluating true positives")
            
    def testTrueNegatives(self):
        '''Be sure shortcut used to count true negatives is correct'''
        for i in range(1000):
            e = np.random.randint(2, size=i)
            g = np.random.randint(2, size=i)
            counter = 0
            for j in range(i):
                if(e[j] == 0 and g[j] == 0):
                    counter += 1
            self.assertEqual(counter, pm.true_negatives(g, e), "Error in evaluating true negatives")
            
    def testFalsePositives(self):
        '''Be sure shortcut used to count false positives is correct'''
        for i in range(1000):
            e = np.random.randint(2, size=i)
            g = np.random.randint(2, size=i)
            counter = 0
            for j in range(i):
                if(e[j] == 0 and g[j] == 1):
                    counter += 1
            self.assertEqual(counter, pm.false_positives(g, e), "Error in evaluating false positives")
            
    def testFalseNegatives(self):
        '''Be sure shortcut used to count false negatives is correct'''
        for i in range(1000):
            e = np.random.randint(2, size=i)
            g = np.random.randint(2, size=i)
            counter = 0
            for j in range(i):
                if(e[j] == 1 and g[j] == 0):
                    counter += 1
            self.assertEqual(counter, pm.false_negatives(g, e), "Error in evaluating false negatives")