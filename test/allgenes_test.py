'''
Created on 14 mag 2016

@author: Luca
'''
import unittest
import src.boltzmann as bm
import numpy as np

class AllgenesTest(unittest.TestCase):
    
    def testDatasetPartitionSize(self):
        '''Be sure the random partition keeps all the elements'''
        dataset = range(20)        
        my_random_partition = bm.random_partition(dataset, 3)     
        length = sum([len(x) for x in my_random_partition])
        self.assertEqual(length, len(dataset), "Partition lengths incorrect")
        
    def testDatasetpartitionContent(self):
        '''Be sure the elements in the partition are the same of the ones in former dataset'''
        dataset = range(20)
        my_random_partition = bm.random_partition(dataset, 5)
        concatenated_partition = [item for sublist in my_random_partition for item in sublist]        
        self.assertEqual(concatenated_partition.sort(), dataset.sort(), "Partition elements are different")
    
    def testHowManySubsets(self):
        '''Be sure you actually get the quantity of subsets you desire from the partition'''
        dataset = range(20)
        my_random_partition = bm.random_partition(dataset, 3)
        self.assertEqual(len(my_random_partition), 3, "Wrong number of subsets in partition")
        
    def testSplitFunction(self):
        '''Be sure split_and_randomize_set returns correct values'''
        for _ in range(1000):
            dataset = np.random.randint(low=0, high=2, size=(500,500))
            lower, higher = bm.split_and_randomize_set(dataset)
            for e in (np.ndarray.sum(lower, axis=0)):
                self.assertLessEqual(e, 10, "First set has elements higher than 10")
            for e in (np.ndarray.sum(higher, axis=0)):
                self.assertGreater(e, 10, "Second set has elements lower than 10")
                
    def testStratifiedPartitionSize(self):
        '''Be sure the random partition keeps all the elements'''
        for _ in range(1000):
            test=np.random.randint(0,2,500)
            self.assertEqual(len(test), sum(map(len, bm.stratified_partitioning(test))), "Partition lengths incorrect")
                       
    def testStratifiedPartitionContent(self):
        '''Be sure the elements in the partition are the same of the ones in former dataset'''
        for _ in range(1000):
            test=np.random.randint(0,2,500)
            my_random_partition = bm.stratified_partitioning(test)
            concatenated_partition = [item for sublist in my_random_partition for item in sublist]        
            self.assertEqual(concatenated_partition.sort(), test.sort(), "Partition elements are different")