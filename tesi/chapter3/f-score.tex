Il calcolo dell'F-score si basa su un insieme di altre misure di performance, che si cerca poi di sintetizzare in un unico valore. Consideriamo un insieme di misure alternative alla percentuale di errore:
\begin{itemize}
	\item i veri positivi (\textit{true positives, tp}) sono i punti (nodi, nel nostro caso) con etichettatura positiva indicati come positivi dal classificatore;
	\item i veri negativi (\textit{true negatives, tn}) sono i nodi con etichettatura negativa che sono indicati come negativi dal classificatore;
	\item i falsi positivi (\textit{false positives, fp}) sono i nodi con etichettatura negativa che sono indicati come positivi dal classificatore;
	\item i falsi negativi (\textit{false negatives, fn}) sono i nodi con etichettatura positiva che sono indicati come negativi dal classificatore.
\end{itemize}
In generale, quando si costruisce un classificatore, lo scopo è sempre quello di massimizzare $tp$ e $tn$ e, di conseguenza, minimizzare $fp$ ed $fn$.

A partire dalle misure sopra elencate è possibile definirne altre derivate; siamo particolarmente interessati a \textit{precision} e \textit{recall}, che ci serviranno sia nel calcolo dell'F-score che successivamente:
\begin{itemize}
	\item precision indica la frazione di nodi, indicati come positivi dal classificatore, che hanno etichetta positiva nel validation set ed è calcolata come
	\begin{equation}	\label{eq:precision}
		P = \frac{tp}{tp + fp}\;;
	\end{equation}
	\item recall, indica la frazione di nodi positivi del validation set che sono classificati come positivi ed è calcolata come
	\begin{equation}	\label{eq:recall}
		R = \frac{tp}{tp + fn}\;.
	\end{equation}
\end{itemize}
L'obiettivo è generalmente quello di massimizzare precision e recall contemporaneamente, portandoli a 1, poiché a valori di precision e recall crescenti corrisponde un numero decrescente di falsi positivi e falsi negativi rispettivamente. Nel caso limite (in cui precision e recall assumono entrambe valore 1)  non si sono presentati falsi positivi né falsi negativi; ciò significa che tutte le predizioni del classificatore corrispondono a veri positivi o veri negativi, ovvero il classificatore utilizzato è un classificatore perfetto per il problema affrontato. L'F-score permette di racchiudere in un solo valore le informazioni di precision e recall ed è calcolato come la media armonica tra questi due valori:
\begin{equation}	\label{eq:fscore}
	F = \frac{2}{\frac{1}{P} + \frac{1}{R}} = \frac{2}{\frac{tp + fp + tp + fn}{tp}} = \frac{2tp}{2tp + fp + fn}\;.
\end{equation}
Come per precision e recall l'obiettivo è quello di ottenere un valore di F-score prossimo a 1, per gli stessi motivi citati poco fa.

Nel Codice \ref{cod:perf-measures} è riportata l'implementazione di queste misure di performance.
\Needspace*{5\baselineskip}
\begin{lstlisting}[caption=Varie misure di performance, label=cod:perf-measures, escapechar=$]
def true_positives(got, expected):
	return sum(np.logical_and(got,expected))

def true_negatives(got, expected):
	return sum(np.logical_not(np.logical_or(got, expected)))

def false_positives(got, expected):
	return sum(np.logical_and(got, np.logical_not(expected)))

def false_negatives(got, expected):
	return sum(np.logical_and(np.logical_not(got), expected))

def precision(got, expected):
	tp = true_positives(got, expected)
	fp = false_positives(got, expected)
	return tp / (tp + fp)

def recall(got, expected):
	tp = true_positives(got, expected)
	fn = false_negatives(got, expected)
	return tp / (tp + fn)

def F_score(got, expected):
	tp = true_positives(got, expected)
	fp = false_positives(got, expected) $\Needspace*{3\baselineskip}$
	fn = false_negatives(got, expected)
	return (2.0 * tp) / (2.0 * tp + fp + fn)
\end{lstlisting}
Nel nostro caso, l'obiettivo del calcolo dell'F-score è quello di mettere alla prova le capacità delle RBM nella classificazione dei positivi.

Per eseguire il calcolo dell'F-score sulle RBM, è stato preparato uno script analogo al Codice \ref{success_rate_exp}, adattato per utilizzare la corretta funzione di calcolo delle performance; in questo caso è particolarmente importante che il partizionamento dei termini avvenga in maniera stratificata, ovvero che i sottoinsiemi siano costruiti in modo tale da contenere lo stesso numero di positivi, al fine di evitare l'eventualità di un set privo di positivi e quindi in grado di generare un'eccezione in seguito a una divisione per 0 nel calcolo dell'F-score.

Nel Codice \ref{cod:fscorecrit} è riportato il criterio utilizzato per calcolare l'F-score dopo aver eseguito la simulazione; mentre la parte rimanente del codice per l'esecuzione dell'esperimento è sostanzialmente identica al Codice \ref{success_rate_exp}.
\Needspace*{5\baselineskip}
\begin{lstlisting}[caption=Criterio per il calcolo dell'F-score dati il vettore simulato e il validation set, label=cod:fscorecrit, escapechar=$]
def criterion(got, expected):
	return [F_score(simulated, expected) for simulated in [(got > t).astype(int)
	         for t in np.arange(0.1, 1, 0.1)]]
\end{lstlisting}
In Tabella \ref{mean:fscore} sono riportati i risultati medi ottenuti dall'esecuzione dell'esperimento.
\begin{table}[t]
	\centering
	\caption{F-score medi nell'apprendimento del dataset}
	\label{mean:fscore}
	\begin{tabular*}{\linewidth}{@{\extracolsep{\fill}}cccc}
		\textbf{Soglia} &
		\begin{tabular}{c}
			\textbf{Valor medio}	\\
			\textbf{(Meno di 10 positivi)}	\\
		\end{tabular} &
		\begin{tabular}{c}
			\textbf{Valor medio}	\\
			\textbf{(Più di 10 positivi)}	\\
		\end{tabular} & 
		\begin{tabular}{c}
			\textbf{Valor medio} 	\\
			\textbf{(Complessivo)}	\\
		\end{tabular} \\\midrule
		0.1 & 0.0016499951 & 0.0157028812 & 0.0086764382	\\
		0.2 & 0.0016499951 & 0.0157028812 & 0.0086764382	\\
		0.3 & 0.0016499951 & 0.0157028812 & 0.0086764382	\\
		0.4 & 0.0016499951 & 0.0157028812 & 0.0086764382	\\
		0.5 & 0.0016748616 & 0.0156923158 & 0.0086835887	\\
		0.6 & 0.0005120632 & 0.0156728278 & 0.0080924455	\\
		0.7 & 0.0000000000 & 0.0150871894 & 0.0075435947	\\
		0.8 & 0.0000000000 & 0.0139334712 & 0.0069667356	\\
		0.9 & 0.0000000000 & 0.0118342860 & 0.0059171430	\\ \hline
	\end{tabular*}
\end{table}
In questo caso i valori ottenuti sono molto bassi, ma coerenti con quanto era stato constatato osservando i risultati del success rate. L'F-score è una metrica che si concentra particolarmente sulla valutazione dei veri positivi riconosciuti dal classificatore, mentre non tiene conto dei veri negativi in alcun modo e per questo non si presta particolarmente a essere utilizzata con campioni di dati binari. Nonostante ciò, questo esperimento ci permette di mettere in luce i limiti del modello nel trattare il campione: per valori di soglia bassi, la maggior parte delle unità nascoste si troverà in stato \textit{on}, massimizzando il numero di veri positivi, ma anche quello di falsi positivi; ricordando ancora una volta che il dataset è prevalentemente negativo, il numero di veri positivi diventa trascurabile rispetto a quello di falsi positivi e le prestazioni diminuiscono drasticamente (questo effetto è ancora più evidente nei termini con meno di 10 positivi, i cui risultati sono di un ordine di grandezza inferiore, esattamente come il numero medio di positivi). Al crescere del valore di soglia, si assiste a una differenza nel comportamento del modello a seconda del tipo di set che si sta trattando: i set con più di 10 positivi, in cui le probabilità di attivazione dei nodi nascosti sono aumentate in seguito all'apprendimento, continuano sostanzialmente a comportarsi allo stesso modo di come avveniva per soglie inferiori, con risultati leggermente ridotti a causa del minor numero di nodi che vengono sogliati a 1; invece nei set con meno di 10 positivi le unità nascoste si trovano tutte nello stato \textit{off} quindi (avendo costruito la partizione in modo che tutti i sottoinsiemi contenessero almeno un positivo) è sufficiente che si presentino uno o più falsi positivi e nessun vero positivo per portare a 0 il risultato.

Osservando la Tabella \ref{tabsr} e la Tabella \ref{mean:fscore} parallelamente, si nota immediatamente che le soglie in corrispondenza delle quali avviene una variazione del risultato sono le stesse; abbiamo quindi ottenuto due visioni diverse dello stesso esperimento, che permettono di valutare il comportamento della rete da due punti di vista differenti.

Nei paragrafi successivi ci occuperemo delle misure che racchiudono in un solo valore una valutazione delle performance del modello.