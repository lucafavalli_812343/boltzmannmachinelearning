All'interno del progetto, le \textit{Strategies} sono esattamente quello che il nome suggerisce: classi che sfruttano il Pattern Strategy \cite{gof} per isolare un algoritmo all'interno di un oggetto, al quale viene delegato un certo comportamento del programma. Tale comportamento può essere modificato a run-time, permettendo un grado di flessibilità fondamentale ai fini della sperimentazione, grazie anche all'utilizzo del \textit{duck typing}.

In fase di apprendimento, saremo interessati nella maggior parte dei casi ad utilizzare una \textit{SigmoidStrategy} con il Decorator \textit{ProbabilityStrategy}; il cui comportamento è illustrato nel \lstref{sigmastrat} e nel \lstref{probstrat}.
\Needspace*{5\baselineskip}
\begin{lstlisting}[caption=SigmoidStrategy, label=sigmastrat, escapechar=$]
class SigmoidStrategy(object):
	
	def __init__(self):
		pass

	def perform(self, layer, weights):	$\Needspace*{3\baselineskip}$
		x = np.dot(layer, weights)
		return vec_sigma(x)
\end{lstlisting}
\Needspace*{5\baselineskip}
\begin{lstlisting}[caption=ProbabilityStrategy, label=probstrat, escapechar=$]
class ProbabilityStrategy(object):
	
	def __init__(self, strategy):
		self.strategy = strategy

	def perform(self, layer, weights):
		x = self.strategy.perform(layer, weights)	$\Needspace*{3\baselineskip}$
		r = np.random.uniform(0, 1, (len(x), len(x[0])))
		return (r <= x).astype(int)
\end{lstlisting}
Utilizzando queste due classi in combinazione, siamo in grado di ottenere lo stesso risultato illustrato nell'\aref{learn}: la \textit{ProbabilityStrategy} chiama la classe decorata \textit{SigmoidStrategy}, la quale si occupa di calcolare la combinazione lineare dei nodi in ingresso con coefficienti pari alle forze della rete, per poi applicare la funzione sigmoide \eqref{eq18}, vettorizzata per poter essere mappata su tutte le posizioni della matrice risultante. Il valore restituito costituisce, nel caso si percorra la rete in direzione dello strato nascosto, la probabilità di attivazione $p(h_{i}=1)$ per ciascuno dei nodi nascosti e per ogni esempio di training; verrà quindi generata una matrice casuale a valori nell'intervallo $[0,1]$ di dimensioni identiche alla precedente e che verrà confrontata con la stessa per simulare l'estrazione bernoulliana con probabilità pari a quella appena citata. Nel complesso, un oggetto \textit{ProbabilityStrategy(SigmoidStrategy)} si comporta come un cluster di unità di soglia sigmoide che lavorano in parallelo per generare le risposte relative a tutti i vettori di training ricevuti in input.

Quando il campione è molto sbilanciato (nel nostro dataset troveremo molte occorrenze di campioni formati quasi unicamente da zeri), la \textit{SigmoidStrategy} tende a favorire fortemente il valore con frequenza relativa predominante; per questo può essere interessante studiare il comportamento della RBM quando le unità di attivazione si adattano per controbilanciare questo effetto, ovvero quando la funzione sigmoide subisce uno shift sull'asse delle ascisse:
\begin{equation} \label{eq23}
	f(x)=\frac{1}{1+\mathrm{e}^{-(x-s)}} \;,
\end{equation}
dove $s\in \mathbb{R}$ rappresenta l'entità dello shift, ottenendo il risultato illustrato in \figref{sigma}(b). Per determinare il valore di $s$, si fissa il valore $\alpha$ pari al rapporto tra positivi e negativi nel set e si risolve rispetto ad $s$ l'equazione:
\begin{figure}[t]
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=\textwidth]{img/sigma.png}
		\caption{Funzione sigmoide $f(x) = \frac{1}{1+\mathrm{e}^{-x}}$.}
		\label{sigma:norm}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=\textwidth]{img/sigmashift.png}
		\caption{Sigmoide shiftata $\bar{f}(x) = \frac{1}{1+\mathrm{e}^{-(x-2)}}$.}
		\label{sigma:shift}
	\end{subfigure}
	\caption{Shift della sigmoide per bilanciare positivi e negativi nel set}
	\label{sigma}
\end{figure}
\begin{equation} \label{eq24}
	\int_{-\infty}^{s}\frac{1}{1+\mathrm{e}^{-x}}\;\mathrm{d}x = \alpha \int_{s}^{\infty}1-\frac{1}{1+\mathrm{e}^{-x}}\;\mathrm{d}x \;.
\end{equation}
Nella \eqref{eq24}, la parte a sinistra dell'uguaglianza rappresenta la probabilità che un nodo nascosto assuma valore 0 quando $x < s$, mentre quella a destra è la probabilità che il nodo nascosto sia attivo quando $x > s$; le probabilità opposte saranno complementari alle precedenti. Riassumendo, poiché nello strato visibile si verifica che il numero di positivi è $\alpha$ volte il quello dei negativi, nello strato nascosto si riequilibrano i risultati rendendo la probabilità di uno 0 pari ad $\alpha$ volte quella di un 1. 
Risolvendo la \eqref{eq24} si ottiene:
$$
	\log (1 + \mathrm{e}^{x})\bigg|_{-\infty}^{s} = \alpha \left(x - \log(1+\mathrm{e}^{x})\right)\bigg|_{s}^{\infty}
$$
\begin{equation} \label{eq25}
	\log (1+\mathrm{e}^{s}) = \alpha \left( \log (1+\mathrm{e}^{s}) -s \right)\;.
\end{equation}
Per ottenere il valore di $s$ a partire dalla \eqref{eq25} viene utilizzato il \lstref{shift}.
\Needspace*{5\baselineskip}
\begin{lstlisting}[caption=Calcolo dello shift sulla sigmoide per bilanciare le probabilità di positivi e negativi in un set fortemente sbilanciato, label=shift, escapechar=$]
def get_shift(alpha):
	f = lambda s: np.log(1 + np.exp(s)) - alpha * (np.log(1 + np.exp(s)) - s) 	$\Needspace*{2\baselineskip}$
	return opt$\footnote{\textit{opt} è il modulo \textit{scipy.optimize}}$.newton(f, 0)
\end{lstlisting}
\par
L'efficacia della strategia che mette in atto questo comportamento, mostrata nel \lstref{shiftstrat}, verrà valutata nel corso degli esperimenti.
\Needspace*{5\baselineskip}
\begin{lstlisting}[caption=ShiftedSigmoidStrategy, label=shiftstrat, escapechar=$]
class ShiftedSigmoidStrategy(object):

	def __init__(self):
		pass
	
	def perform(self, layer, weights):
		x = np.dot(layer, weights)
		alphas = np.array([float(positives(row)) / negatives(row) for row in layer])
		s = np.array([map(get_shift, alphas)] * len(x[0])).transpose()
		return vec_sigma(x - s)
\end{lstlisting}

Mentre le strategie viste finora risultano fondamentali nello svolgimento dei processi di apprendimento e di simulazione, in alcuni casi siamo interessati più a valutare le probabilità di attivazione dei nodi e l'accuratezza del risultato rispetto al valore atteso per diversi valori di soglia, oppure con una particolare funzione di confronto. Per questo, nella maggior parte degli esperimenti, in fase di simulazione verranno usate Strategies di diverso tipo, che combinano il comportamento di una delle strategie viste precedentemente (spesso verrà calcolata la probabilità applicando la funzione sigmoide, senza però effettuare alcun tipo di sogliatura) con quelle di una funzione per confrontare il valore simulato con quello atteso. A queste strategie è stato assegnato il nome di \textit{SimulationStrategy}.
\Needspace*{7\baselineskip}
\begin{lstlisting}[caption=SimulationStrategy, label=simstrat, escapechar=$]
class SimulationStrategy(object):

	def __init__(self, thresholding_strategy, criterion):
		self.thresholding_strategy = thresholding_strategy
		self.criterion = criterion

	def perform(self, rbm, visible, hidden):
		rbm.change_strategy(self.thresholding_strategy)	$\Needspace*{3\baselineskip}$
		simulated = rbm.simulate_hidden(visible)
		return self.criterion(simulated, hidden)
\end{lstlisting}
L'esecuzione del metodo \textit{perform} di un oggetto definito come nel \lstref{simstrat} passa attraverso una serie di passaggi: viene aggiornato il comportamento della RBM per adottare la nuova strategia, lanciata la simulazione e infine confrontato il valore ottenuto con quello atteso per determinare il risultato.