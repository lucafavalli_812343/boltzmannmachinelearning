Il primo esperimento effettuato consiste nell'esecuzione della procedura di cross-validation descritta nel Paragrafo \ref{sssecexp}, per poi confrontare lo stato dei nodi all'uscita della rete (strato nascosto) con i valori del validation set, cercando una corrispondenza uno a uno. In breve, la funzione di valutazione delle performance valuta la frequenza relativa con cui un nodo dello strato simulato si trova nello stesso stato del validation set. Questa funzione è mostrata nel Codice \ref{success_rate_fun}.
\Needspace*{5\baselineskip}
\begin{lstlisting}[caption=Success rate di uno strato simulato rispetto a quello atteso, label=success_rate_fun, escapechar=$]
def success_rate(got, expected):
	return sum(expected == got)/float(len(got))
\end{lstlisting}
Invece di applicare una soglia casuale alle probabilità di attivazione dei nodi, è stato deciso di sogliare tutti i nodi contemporaneamente con un valore di soglia identico, ma variabile nel range $[0.1, 0.9]$, con step di 0.1; sono state quindi applicate nove diverse soglie e riportati i risultati per ciascuna di esse. L'intera procedura è stata ripetuta 25 volte su 100 colonne casuali della matrice di \textit{Gene Ontology}: 50 estratte tra quelle con meno di 10 positivi e 50 tra quelle con più di 10 positivi. Lo script che esegue questo esperimento è riportato nel Codice \ref{success_rate_exp}.
\Needspace*{5\baselineskip}
\begin{lstlisting}[caption=Esperimento per calcolare il success rate dell'apprendimento di 100 termini casuali, label=success_rate_exp, escapechar=$]
dataset = load_data('../options/allgenes-dataset/' +
					  'yeast.04_07_15_annotation.MF.allgenes.txt', '\t')

_, less_indexes, _, more_indexes = split_and_randomize_set(dataset=dataset,
																  min_positives=0)
less_indexes.sort()
more_indexes.sort()

def criterion(got, expected):
	return [success_rate(simulated, expected) for simulated in
			 [(got > t).astype(int) for t in np.arange(0.1, 1, 0.1)]]

def run_experiment(output_file, indexes, metrics, learning_strategy,
				 	 simulation_strategy, loops=25):
	columns = ['Molecular Function'] + map(str, np.arange(0.1, 1, 0.1))
	with open(output_file, 'w+') as out:
		out.write(','.join(columns))
		for i in range(len(indexes)):
			values = []
			for _ in range (loops):
				exp = Experiment(dataset[:, indexes[i]], metrics=metrics,
								   simulation_strategy=simulation_strategy,
								   learning_strategy=learning_strategy)
				values.append([exp.cross_validation(
							    partition_function = stratified_partitioning)])
				out.write('\n%d,%s' % (indexes[i], ','.join(map(
						   lambda x: '{0:.10f}'.format(x), np.mean(values, axis=0)))))

metrics = LearningMetrics([], convergence_threshold=10e-3)

'''FIRST TEST'''
run_experiment('../../results/success_rate_less.csv', less_indexes,
				 metrics, ProbabilityStrategy(SigmoidStrategy()),
				 SimulationStrategy(SigmoidStrategy(), criterion))
$\Needspace*{5\baselineskip}$
'''SECOND TEST'''
run_experiment('../../results/success_rate_more.csv', more_indexes,
			 	 metrics, ProbabilityStrategy(SigmoidStrategy()),
				 SimulationStrategy(SigmoidStrategy(), criterion))
\end{lstlisting}
I risultati medi dell'esecuzione dell'esperimento appena descritto sono riportati in Tabella \ref{tabsr}: nella prima colonna sono indicati i valori di soglia applicati, nella seconda è possibile osservare i risultati medi relativi ai 50 termini estratti casualmente dalla classe con meno di 10 positivi, nella terza quelli relativi ai 50 termini della classe con più di 10 positivi, mentre nell'ultima la media complessiva su tutte le esecuzioni. Una rappresentazione analoga verrà utilizzata in corrispondenza delle tabelle successive, riportanti i risultati per gli altri esperimenti.

Nel caso in cui il lettore desideri consultare le tabelle contenenti tutti i risultati ottenuti (a partire dai quali sono state calcolate le medie della Tabella \ref{tabsr} e di tutti i successivi esperimenti), si rimanda alla lettura dei file presenti all'interno della cartella \textit{results} del repository del progetto.

Dai risultati riportati in Tabella \ref{tabsr}, appare evidente che il success rate è molto basso per valori di soglia inferiori allo 0.5 e cresce rapidamente per i valori successivi. Per poter interpretare i risultati è necessario ricordare brevemente il significato degli output di una RBM, il modo in cui essa apprende e l'entità del dataset considerato.

\begin{table}[t]
	\centering
	\caption{Success rate medi nell'apprendimento del dataset}
	\label{tabsr}
	\begin{tabular*}{\linewidth}{@{\extracolsep{\fill}}cccc}
		\textbf{Soglia} &
		\begin{tabular}{c}
			\textbf{Valor medio}	\\
			\textbf{(Meno di 10 positivi)}	\\
		\end{tabular} &
		\begin{tabular}{c}
			\textbf{Valor medio}	\\
			\textbf{(Più di 10 positivi)}	\\
		\end{tabular} & 
		\begin{tabular}{c}
			\textbf{Valor medio} 	\\
			\textbf{(Complessivo)}	\\
		\end{tabular} \\ \midrule
		0.1 & 0.0008258097 & 0.0080076805 & 0.0044167451	\\
		0.2 & 0.0008258097 & 0.0080076805 & 0.0044167451	\\
		0.3 & 0.0008258097 & 0.0080076805 & 0.0044167451	\\
		0.4 & 0.0008258097 & 0.0080076805 & 0.0044167451	\\
		0.5 & 0.2449244719 & 0.0651477356 & 0.1550361037	\\
		0.6 & 0.9465853991 & 0.2804895447 & 0.6135374719	\\
		0.7 & 0.9991741903 & 0.4852326867 & 0.7422034385	\\
		0.8 & 0.9991741903 & 0.6315231160 & 0.8153486532	\\
		0.9 & 0.9991741903 & 0.7678033215 & 0.8834887559	\\ \hline
	\end{tabular*}
\end{table}

Per prima cosa, sapendo che i pesi iniziali della rete sono tutti pari a 0, la prima combinazione lineare calcolata dalla rete sarà necessariamente pari a 0 per tutti i nodi in uscita; applicando la funzione sigmoide, la probabilità di attivazione corrispondente sarà $p(k(h)=1)=\frac{1}{2}\;\forall h \in H$, cosa sensata per una rete non addestrata. Da questo punto in avanti, le iterazioni dell'algoritmo di apprendimento porteranno ad aggiornare i pesi, tramite l'applicazione della \eqref{eq15}, fino a convergenza.

Osserviamo, tuttavia, che la dimensione di un qualsiasi termine del dataset considerato è la seguente:
\begin{lstlisting}[frame=none, numbers=none]
	In [1]:	len(dataset[:, 0])
	Out[1]:	6224
\end{lstlisting}
mentre il numero medio di positivi in ciascuno dei gruppi considerati (con meno di 10 positivi e con più di 10 positivi) e complessivo è rispettivamente:
\begin{lstlisting}[frame=none, numbers=none]
	In [2]:	np.mean([sum(dataset[:, i]) for i in less_indexes])
	Out[2]:	5.14

	In [3]:	np.mean([sum(dataset[:, i]) for i in more_indexes])
	Out[3]:	49.84
	
	In [4]:	np.mean([sum(dataset[:, i])
				for i in np.concatenate((less_indexes, more_indexes))])
	Out[4]:	27.49	
\end{lstlisting}
Stiamo quindi trattando una matrice di grosse dimensioni, ma fortemente sparsa\footnote{Una matrice sparsa è una matrice in cui la maggior parte dei valori è uguale a 0.}, anche estraendo casualmente termini con un numero minimo di positivi. Questa caratteristica del dataset è una delle criticità principali incontrate nell'utilizzo del modello di RBM proposto e diventerà sempre più evidente mano a mano che procederemo con le osservazioni e i successivi esperimenti.

Ciascun valore ottenuto nello strato nascosto in seguito alla simulazione indica la probabilità che il nodo corrispondente si trovi nello stato \textit{on}, calcolata applicando la funzione sigmoide sulla combinazione lineare dei nodi visibili pesata con le forze delle connessioni in uscita dalla rete. Date le osservazioni sui valori del dataset appena fatte, possiamo assumere (per semplificazione) che il success rate cresca in maniera proporzionale al numero di zeri prodotti dalla rete. Questa constatazione trova riscontro nella Tabella \ref{tabsr}, che riporta valori molto più alti in corrispondenza di soglie maggiori, ovvero quando la maggior parte delle probabilità in uscita dalla rete viene sogliata a 0. Partendo da questo presupposto, il fatto che i risultati (nel caso dei set con meno di 10 positivi, presenti nella seconda colonna della tabella) crescano immediatamente dopo aver superato la soglia di 0.5 sottolinea che la probabilità di attivazione dei nodi nascosti è prossima al 50\% anche dopo che l'apprendimento è stato ultimato, cioè che i pesi della rete hanno subito solamente minime variazioni rispetto al valore iniziale, mentre sembrerebbe ragionevole pensare che le probabilità siano fortemente diminuite in seguito all'apprendimento di un training set prevalentemente negativo. Questa incongruenza è ancora più marcata nei set con più di 10 positivi (terza colonna della tabella), in cui i valori di probabilità sono più sbilanciati verso 1. Per comprendere le motivazioni alla base di questo comportamento, rivediamo il significato delle quantità che prendono parte al calcolo dell'aggiornamento dei pesi:
\begin{itemize}
	\item $\left\langle v_{i}h_{j} \right\rangle_{data} = p(k(v_{i})k(h_{j})=1)$ quando si percorre la rete dallo strato visibile a quello nascosto, ovvero calcolando i feature detector a partire dagli esempi di training;
	\item $\left\langle v_{i}h_{j} \right\rangle_{recon} = p(k(v_{i})k(h_{j})=1)$ quando si percorre la rete dallo strato nascosto a quello visibile, ovvero quando l'uscita della rete ottenuta al punto precedente viene usata come input per percorrere la rete in direzione opposta.
\end{itemize}
Osserviamo immediatamente che, poiché in questo caso si sta usando un solo vettore di training (costituito dall'unione di tutti i sottoinsiemi in cui è stato suddiviso il termine del dataset, a esclusione di quello scelto come validation set per l'attuale iterazione della cross-validation), $\left\langle v_{i}h_{j} \right\rangle_{data}$ e $\left\langle v_{i}h_{j} \right\rangle_{recon}$ possono valere solamente 1 oppure 0. Inoltre, entrambi questi valori sono influenzati esclusivamente dalle coppie di positivi nella simulazione, mentre non cambiano per le coppie di negativi. Come discusso precedentemente, essendo la frequenza relativa di positivi nel set molto bassa, è altrettanto improbabile che si presentino dei valori in grado di contribuire all'incremento di $\left\langle v_{i}h_{j} \right\rangle_{data}$ e $\left\langle v_{i}h_{j} \right\rangle_{recon}$ e, di conseguenza, delle corrispondenti matrici nel Codice \ref{training}; in questa situazione, diventa possibile che la variazione di pesi sia molto bassa o addirittura nulla e l'algoritmo termini prematuramente.

Osservando la Tabella \ref{tabsr}, i risultati relativi alla colonna ``più di 10 positivi'', pur rimanendo estremamente sbilanciati, subiscono un incremento leggermente più graduale rispetto ai set con meno di 10 positivi e si assestano su valori di un ordine di grandezza più alti anche per le soglie più piccole: questo conferma che l'algoritmo è fortemente influenzato dai valori positivi e per nulla da quelli negativi, tanto che l'apprendimento di un set con un numero maggiore di positivi porta addirittura ad un peggioramento complessivo delle prestazioni.

Nel prossimo paragrafo confermeremo ulteriormente questa ipotesi, attraverso l'utilizzo di una diversa misura di performance: l'F-score.