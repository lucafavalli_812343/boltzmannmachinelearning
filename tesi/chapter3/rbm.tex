Le macchine di Boltzmann ristrette utilizzate in questo progetto sono reti neurali costituite da due livelli di neuroni che possono assumere valori binari: ciascun nodo di uno strato è connesso bidirezionalmente con tutti e soli i nodi dello strato opposto e le forze di tali connessioni vengono aggiornate eseguendo un algoritmo di apprendimento tramite osservazione e scoperta non supervisionato; inoltre, è possibile creare uno stack di RBM per costruire reti neurali multilivello. Per gestire strutture di questo tipo è stata concepita una classe apposita che rispetta tutte le caratteristiche sopra elencate, inserendo una serie di funzionalità aggiuntive interessanti.

Un oggetto di tipo \textit{RBM} è descritto da tre attributi: un oggetto di tipo \textit{LearningMetrics}\footnote{In Python è possibile passare, come parametro all'interno di una funzione, oggetti di qualsiasi tipo, purché dispongano di specifici metodi che verranno loro richiesti (vedi Paragrafo \ref{sssecpy}). Per questo motivo, non ha senso dire che una funzione prende in ingresso oggetti di un certo tipo; tuttavia, in questa sede, queste parole verranno usate per indicare che, relativamente all'implementazione scelta e agli esperimenti effettuati, è consigliabile che il parametro in considerazione sia del tipo indicato per avere un corretto funzionamento del programma.} e una strategy, che vengono inizializzati quando viene chiamato il costruttore di RBM, e infine una lista di matrici \textit{weights} contenente la forza delle connessioni tra i nodi nascosti e quelli visibili per ciascuno dei livelli della RBM.
\Needspace*{2\baselineskip}
\begin{lstlisting}[caption=Inizializzazione di una RBM, label=init, escapechar=$]
def __init__(self, training_set, metrics, strategy):
	self.metrics = metrics	$\Needspace*{3\baselineskip}$
	self.strategy = strategy
	self.learn(training_set) 
\end{lstlisting}
Gli oggetti \textit{LearningMetrics} contengono tutte le informazioni utili relative all'attuale esecuzione e metodi per la loro gestione; rappresentano quindi un'implementazione di Pattern Façade \cite{gof} che racchiude lo stato della macchina e ne fornisce un'interfaccia intuitiva. In particolare, siamo interessati ad alcune delle costanti fondamentali che governano l'algoritmo di apprendimento:
\begin{itemize}
	\item \textit{hidden\_dimensions}: una lista di interi in cui ciascuna posizione rappresenta il numero di nodi nascosti da cui è costituita la RBM multilivello al livello corrispondente (ovviamente, in RBM formate da un singolo strato visibile e uno nascosto, questa lista avrà un solo elemento);
	\item \textit{learning\_rate}: un valore che rappresenta il fattore moltiplicativo subito dagli aggiornamenti dei pesi a ciascuna iterazione; corrisponde alla $\epsilon$ della \eqref{eq14} e dell'\aref{learn};
	\item \textit{convergence\_threshold}: rappresenta il valore di soglia sotto al quale fermare l'apprendimento; quando la massima variazione di peso nella rete è inferiore a tale valore, l'algoritmo termina; corrisponde alla $t$ dell'\aref{learn}.
\end{itemize}

Le strategy, invece, hanno il requisito particolare di possedere un metodo \textit{perform} che permetta l'esecuzione della strategia stessa; esse vengono utilizzate quando si percorre la rete, per generare lo stato dei nodi in base all'input ricevuto e risultano particolarmente comode in situazioni in cui è necessario cambiare strategia durante l'esecuzione (vedi Parafrafo \ref{sssecstrat}). 

Come riportato nel \lstref{init}, durante la fase di inizializzazione della RBM, viene anche lanciato l'algoritmo di apprendimento che, preso in ingresso il training set, si occuperà di inizializzare i valori di \textit{weights}. Esso rappresenta la traduzione in codice dell'\aref{learn}, ottimizzato per sfruttare le operazioni tra matrici.

\input{chapter3/learn.tex}

Il \lstref{training} rappresenta il fulcro principale attorno al quale ruota l'intera struttura delle RBM.

Nelle righe 3-14 avviene l'inizializzazione di tutte le variabili che verranno utilizzate nell'algoritmo; in particolare alcune sono estratte dalle \textit{metrics} dell'istanza, mentre altre vengono create a partire dai dati disponibili, ad esempio nelle righe 8-13 il codice produce lista di matrici dei pesi per ogni livello, con valore di partenza a 0 per ogni posizione.

La seconda parte del metodo \textit{learn} contiene tutte le operazioni necessarie all'aggiornamento dei pesi della rete: per ciascun livello della RBM si ripete fino a convergenza (ovvero fino a quando si verifica $d\_max < convergence\_threshold$) il calcolo dei feature detector ($drive\_feature\_detectors$) per ottenere lo stato dei nodi nello strato opposto. Tali valori verranno utilizzati per calcolare $\left\langle v_{i}h_{j} \right\rangle_{data}$ e $\left\langle v_{i}h_{j} \right\rangle_{recon}$, necessarie per il calcolo della variazione dei pesi, tramite i prodotti alle righe 26 e 27 del \lstref{training}, che permettono di raggiungere in una sola operazione il duplice obiettivo di calcolare quando entrambi gli strati della rete siano \textit{on} e contare il numero di occorrenze in cui tale evento si verifica, come indicato nel commento alla \eqref{eq14}. Questo è possibile grazie alla natura binaria delle matrici: poiché le posizioni delle matrici possono assumere solamente valori 0 e 1, il loro prodotto sarà 1 solamente quando entrambi i nodi sono attivi nell'attuale configurazione.

Il metodo $drive\_feature\_detectors$ non fa altro che eseguire la strategy della RBM, che generalmente consisterà nel prodotto righe per colonne tra le matrici in ingresso (il prodotto tra matrici corrisponde all'applicazione della \eqref{eq17} su tutte le possibili coppie di nodi), la mappatura della funzione sigmoide sui risultati ed eventualmente l'applicazione di una soglia.

Superata la \textit{threshold} per il livello attuale, la matrice dei pesi viene utilizzata per generare un'ultima volta una rappresentazione nascosta del training set; essa costituirà il training set per il livello successivo. Il sistema è quindi già predisposto alla costruzione di reti neurali profonde, sia per quanto riguarda l'aggiornamento dei pesi che per la generazione delle feature.

Appare a questo punto evidente uno dei più grandi vantaggi dell'utilizzo delle RBM per la risoluzione dei problemi di \textit{Machine Learning}: giunta a convergenza, la rete è interamente descritta da una matrice (per le RBM a singolo livello), o al più una lista di matrici, senza necessitare di strutture dati più complesse, ad esempio grafi (anche i grafi possono essere implementati molto semplicemente, ma su di essi risulterebbero molto più dispendiose le operazioni sopra descritte). Tale modello si presta anche a efficaci ottimizzazioni su sistemi distribuiti, per i quali esistono implementazioni in grado di eseguire parallelamente le operazioni tra matrici: l'algoritmo divide et impera per la moltiplicazione tra matrici ne è un tipico rappresentante. Questo è solo un esempio delle direzioni verso le quali sarebbe possibile estendere lo studio effettuato nell'ambito del tirocinio, argomento discusso più approfonditamente nel Paragrafo \ref{future}.

Una volta che il processo di apprendimento è terminato, la rete con i pesi aggiornati può essere utilizzata per effettuare delle simulazioni: preso in ingresso un vettore dei nodi visibili, la rete viene percorsa per ottenere la rappresentazione nascosta dello stesso vettore, in un processo di riconoscimento; viceversa, preso in ingresso un vettore nascosto, la rete viene percorsa in direzione opposta per sfruttare le capacità di generazione della rete (\lstref{simulate}).
\Needspace*{5\baselineskip}
\begin{lstlisting}[caption=Generazione di vettori visibili e nascosti in una RBM, label=simulate, escapechar=$]
def simulate_hidden(self, visible):
    for layer in self.weights:
	    visible = self.drive_feature_detectors([visible], layer)
    return visible[0].tolist()


def simulate_visible(self, hidden):   
    for layer in self.weights[::-1]:    $\Needspace*{3\baselineskip}$
	    hidden = self.drive_feature_detectors([hidden], layer.transpose())
    return hidden[0].tolist()
\end{lstlisting}

Finora non abbiamo considerato il valore di soglia $b_{j}$ associato a ciascun nodo $j$ della RBM, ovvero il peso dell'arco che collega un nodo con se stesso, che, come indicato nella \eqref{eq17}, rientra nel calcolo dei feature detectors. Come già discusso nel Paragrafo \ref{ssecrbm}, nella maggior parte dei casi il miglior modo di implementare questa funzionalità consiste nel dotare la rete di un nodo immaginario aggiuntivo, detto nodo di bias o nodo soglia, il cui stato di attivazione è sempre pari a $1$ in qualsiasi configurazione che la rete può assumere, in modo da ottenere la struttura rappresentata in \figref{biasnode}. Nel nostro caso, è sufficiente aumentare di 1 la dimensione di entrambi gli strati visibile e nascosto, in modo che la connessione aggiuntiva rappresenti quella del nodo con se stesso (o, analogamente, con il nodo immaginario). Perché l'apprendimento proceda correttamente, sarà necessario che ogni esempio di training riporti un $1$ aggiuntivo nell'ultima posizione, in modo da rispettare il modello appena descritto.
\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{img/bias.png}
	\captionof{figure}{RBM dotata di un nodo di bias} \label{biasnode}
\end{figure}

Mentre le forze delle connessioni con il nodo di bias costituiscono un'utile rappresentazione interna ai fini dell'apprendimento e della simulazione, non è altrettanto importante lo stato di attivazione assunto dal nodo in seguito alla generazione di vettori allo stato di equilibrio, ottenuti percorrendo la rete su input visibile o nascosto; per questo, prima di restituire l'output, lo stato del nodo di bias verrà scartato. Per implementare reti di questo tipo, ci siamo appoggiati alle RBM sopra descritte, estendendole grazie alle proprietà dell'ereditarietà tra classi; la classe in questione è interamente riportata nel \lstref{bias}.
\Needspace*{5\baselineskip}
\begin{lstlisting}[caption=Implementazione di una RBM dotata di un nodo di bias, label=bias, escapechar=$]
class BiasedRBM(RBM):

	def __init__(self, training_set, metrics, strategy):
		training_set = np.array([list(e) + [1] for e in training_set])
		metrics.hidden_dimensions = [value+1 for value in metrics.hidden_dimensions]
		super(BiasedRBM, self).__init__(training_set, metrics, strategy) 
	
	def simulate_hidden(self, visible):
		visible = visible + [1]
		return super(BiasedRBM, self).simulate_hidden(self, visible)[:-1:]    
	$\Needspace*{4\baselineskip}$
	def simulate_visible(self, hidden):
		hidden = hidden + [1]
		return super(BiasedRBM, self).simulate_visible(self, hidden)[:-1:]
\end{lstlisting}