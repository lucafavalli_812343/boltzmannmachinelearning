La parola \textit{intelligenza} può assumere molti significati e spesso non è possibile darne una definizione univoca. In generale, tuttavia, quasi tutte le definizioni citano tra i requisiti fondamentali la \textit{capacità di adattamento}; l'essere intelligente deve essere consapevole dell'ambiente circostante e saper sfruttare tale consapevolezza per modificare il proprio comportamento in modo vantaggioso. Perché questo avvenga, l'essere deve essere dotato della capacità di imparare. Risulta quindi evidente che, nel campo delle intelligenze artificiali, molti tentativi siano andati nella direzione della costruzione di macchine (sia hardware che software) in grado di imparare. Questo tipo di ricerca si pone una serie di obiettivi che spaziano dalla semplice utilità pratica (sistemi in grado di apprendere costituiscono un'enorme rivoluzione in tutti gli ambiti produttivi e lavorativi in generale), fino alla biologia, dove lo studio di macchine intelligenti rappresenta prima di tutto un modo per capire meglio il comportamento del nostro stesso cervello e, a livello più astratto, il concetto stesso di intelligenza.

La ricerca nel campo del \textit{Machine Learning} si occupa di diversi paradigmi relativamente alla problematica dell'apprendimento, ma in questa sede ci soffermeremo solamente sul \textit{Neural Modelling}, paradigma seguito dalle RBM. Il \textit{Neural Modelling} si occupa di costruire un sistema general purpose che sia in grado di adattarsi a specifici task, il cui il comportamento è determinato al momento dell'apprendimento, tramite la modifica delle probabilità con cui i neuroni sono attivati; tuttavia, non possedendo alcuna conoscenza pregressa, non è sempre possibile costruire conoscenza ad alto livello di astrazione. I sistemi di questo tipo vengono usati principalmente nei problemi di classificazione, come la pattern recognition \cite{aarts}.

Le capacità di apprendimento di cui una macchina di Boltzmann è dotata rientrano sostanzialmente in due categorie: \textit{apprendimento tramite esempi} e \textit{apprendimento tramite osservazione e scoperta}.

L'apprendimento tramite esempi è un tipo di apprendimento supervisionato in cui alla rete vengono sottoposti (da un ipotetico ``maestro'') un insieme di esempi e controesempi costituenti la rappresentazione di uno o più concetti, dei quali la rete (l'``allievo'') induce una descrizione. Quando sfruttate in questo modo, le macchine si prestano principalmente all'utilizzo come classificatori, ma apprendono come ``effetto collaterale'' capacità generative: se percorsa in direzione opposta (l'output diventa input e viceversa), cosa assolutamente possibile nelle macchine di Boltzmann grazie alla natura bidirezionale delle loro connessioni, la rete è in grado di generare un esempio appartenente al gruppo ricevuto come input. Sottoporre alla macchina la coppia $(\textbf{e},\textbf{c})$, dove $\textbf{e}$ è l'esempio e $\textbf{c}$ la sua classificazione, riduce l'energia $E(\textbf{e},\textbf{c})$, ovvero aumenta la probabilità $p(\textbf{v},\textbf{h})$, tramite la modifica dei pesi $s_{v,h}$. Questo significa che la rete assocerà all'input $\textbf{e}$ la classificazione $\textbf{c}$ con maggiore probabilità. Ripetere lo stesso procedimento per tutti gli esempi, accompagnati dalla relativa classificazione, porta alla generazione della funzione consenso che caratterizza la macchina di Boltzmann.

A livello puramente teorico, l'apprendimento si basa sui concetti di \textit{comportamento globale} e \textit{divergenza}.
\begin{definition} [{\cite{aarts}}] \label{def6} \definitions{Comportamento globale}
	Sia $\mathcal{B} = (\mathcal{U}, \mathcal{C})$ una macchina di Boltzmann. Il suo \textit{comportamento globale} è la distribuzione stazionaria $\textbf{q}(c)$ che definisce su tutte le $k \in \mathcal{R}$ la probabilità di raggiungere l'equilibrio; le sue componenti sono definite come:
	\begin{equation} \label{eq10}
		q_{k}(c) = \frac{1}{N_{0}(c)} \exp\left( \frac{\mbox{C}(k)}{c} \right)\;,
	\end{equation}
	dove $c$ è un parametro di controllo, $\mbox{C}(k)$ è definita come nella \eqref{eq9} e $N_{0}(c) = \sum\limits_{l \in \mathcal{R}} \exp\left( \frac{\mathrm{C}(l)}{c} \right)$.
\end{definition}
\begin{definition} \label{def7} \definitions{Isomorfismo}
	Due macchine di Boltzmann $\mathcal{B} = (\mathcal{U}, \mathcal{C})$ e $\mathcal{B'} = (\mathcal{U'}, \mathcal{C'})$ si dicono \textit{isomorfe} se $\mathcal{U}=\mathcal{U'}$ e $\forall c_{u,v} = \{u,v, s_{u,v}\} \in \mathcal{C},\;\exists c'_{u,v}=\{u,v,s'_{u,v}\}\in\mathcal{C'}$.
\end{definition}
\begin{definition}[{\cite{aarts}}] \label{def8} \definitions{Divergenza}
	Siano $\mathcal{B}$ e $\mathcal{B'}$ due macchine di Boltzmann isomorfe con comportamento globale $\textbf{q}(c)$ e $\textbf{q}'(c)$ rispettivamente. La \textit{divergenza} $D(\textbf{q}|\textbf{q}')$ di \textbf{q}(c) rispetto a \textbf{q'}(c) è data da:
	\begin{equation} \label{eq11}
		D(\textbf{q}|\textbf{q}') = \sum\limits_{k \in \mathcal{R}} q'_{k}(c) \ln \frac{q'_{k}(c)}{q_{k}(c)}\;,
	\end{equation}
	dove le $q_{k}(c)$ e $q'_k(c)$ sono le componenti delle distribuzioni stazionarie $\textbf{q}(c)$ e $\textbf{q}'(c)$ rispettivamente.
\end{definition}

\uppercase{è} possibile definire l'apprendimento come un processo che, dato un comportamento globale desiderato $\textbf{q}'(c)$, minimizza la divergenza $	D(\textbf{q}|\textbf{q}')$ del comportamento globale $\textbf{q}(c)$ di $\mathcal{B}$ rispetto a $\textbf{q}'(c)$.

Per farlo, partendo da un vettore delle forze $\textbf{s}^{(0)} \in \mathbb{R}^{|\mathcal{C}|}$, formato dalle forze iniziali $s_{u,v}$ delle connessioni della rete, l'algoritmo di apprendimento genera una sequenza $\left\lbrace \textbf{s}^{(k)} \right\rbrace$, $k=1,2,3,\dots$, costruita iterativamente come:
\begin{equation} \label{eq12}
	\textbf{s}^{(i+1)} = \textbf{s}^{(i)} - \beta \nabla D(\textbf{q}^{(i)}|\textbf{q}') \;,
\end{equation}
dove $\textbf{q}^{(i)}$ determina il comportamento globale della rete rispetto a $\textbf{s}^{(i)}$, $\beta \in \mathbb{R}^{+}$ è una costante arbitraria e $\nabla D(\textbf{q}^{(i)}|\textbf{q}')$ è il gradiente della divergenza. Notiamo che il processo di apprendimento si muove in direzione opposta al gradiente, ovvero cerca di ridurre la divergenza tra $\textbf{q}(c)$ e $\textbf{q}'(c)$. Tale processo è \textit{stabile} se, data una funzione $f$ definita su $\mathbb{R}^{|\mathcal{C}|}$:
\begin{equation} \label{eq13}
	f(\textbf{s}^{(i+1)}) \leq f(\textbf{s}^{(i)})
\end{equation}
$\forall i \in \mathbb{N}$. Come dimostrato in \fcite{aarts}{218}, il processo di apprendimento è stabile (rispetta la \eqref{eq13}) quando si sceglie $\beta \leq \frac{c^{2}}{|\mathcal{C}|}$.

L'apprendimento tramite osservazione e scoperta avviene senza supervisione e scarica sulla macchina la responsabilità di individuare le regolarità del campione e di modificare la propria rappresentazione interna al fine di sviluppare capacità associative; reti di questo tipo presentano anche comportamenti tipici della \textit{content addressable memory}. I modelli di memoria associativi apprendono un insieme e sono successivamente utilizzati per completare un pattern passato in input solo parzialmente. Reti di questo tipo possiedono solo unità di input e nascoste, ma nessun nodo di output e sono il modello utilizzato in questo progetto.

L'apprendimento in una RBM che sfrutta questa strategia si basa su una regola di aggiornamento molto semplice \cite{hint}\cite{hint3}:
\begin{equation} \label{eq14}
	\Delta w_{ij} = \epsilon \left( \left\langle v_{i}h_{j} \right\rangle_{data} - \left\langle v_{i}h_{j} \right\rangle_{recon} \right) \;,
\end{equation}
dove $\Delta w_{i,j}$ è la variazione del peso dell'arco che collega $i$ e $j$ (useremo quindi da qui in avanti il peso $w_{ij}$ come sinonimo della forza $s_{i,j}$ vista precedentemente), $\epsilon$ è una costante chiamata \textit{learning rate} (equivalente alla $\beta$ della \eqref{eq12}), $\left\langle v_{i}h_{j} \right\rangle_{data}$ è la frequenza con cui il pixel (nodo visibile) $i$ e il feature detector (nodo nascosto) $j$ sono \textit{on} contemporaneamente quando si percorre la rete dallo strato visibile a quello nascosto (ovvero la connessione $c_{i,j}$ è attivata nella configurazione) e viceversa $\left\langle v_{i}h_{j} \right\rangle_{recon}$ rappresenta la stessa frequenza quando si percorre la rete nella direzione opposta. La \eqref{eq14} rappresenta l'approssimazione di un'altra grandezza:
\begin{equation} \label{eq15}
	\Delta w_{ij} = \epsilon \left( \left\langle v_{i}h_{j} \right\rangle_{data} - \left\langle v_{i}h_{j} \right\rangle_{model} \right) \;,
\end{equation}
ottenuta a partire dalla regola derivativa del logaritmo della probabilità di un vettore di training vista nella \eqref{eq8}:
\begin{equation} \label{eq16}
	\frac{\delta\log p(\textbf{v})}{\delta w_{ij}} = \left\langle v_{i}h_{j} \right\rangle_{data} - \left\langle v_{i}h_{j} \right\rangle_{model} \;.
\end{equation}
La \eqref{eq14} è preferita a causa della semplicità di calcolo, nonostante sia una migliore approssimazione della \textit{Contrastive Divergence}\footnote{La \textit{Contrastive Divergence} \cite{hint3} è un metodo per approssimare il gradiente di una funzione energia negli algoritmi di apprendimento.} rispetto a quanto lo sia della \eqref{eq16} \cite{hint3}.

Per applicare la \eqref{eq14} è necessario prima di tutto valutare i valori di $\left\langle v_{i}h_{j} \right\rangle_{data}$ e  $\left\langle v_{i}h_{j} \right\rangle_{recon}$. A causa della struttura della RBM, questo processo è molto semplice:
\begin{equation} \label{eq17}
	p(h_{j} = 1 | \textbf{v}) = \sigma \left(b_{j} + \sum\limits_{i=1}^{|V|} v_{i}w_{ij}\right) \;,
\end{equation}
dove $\sigma$ rappresenta la funzione di attivazione sigmoide
\begin{equation} \label{eq18}
	\sigma(x) = \frac{1}{1 + e^{-x}} \;
\end{equation}
e $b_{j}$ il valore di bias relativo al nodo nascosto $j$; lo stesso vale percorrendo la rete nella direzione opposta:
\begin{equation} \label{eq19}
p(v_{i} = 1 | \textbf{h}) = \sigma \left(b_{i} + \sum\limits_{j=1}^{|H|} h_{i}w_{ij}\right) \;;
\end{equation}
nel primo caso otterremo come risultato i valori di probabilità utili al calcolo di $\left\langle v_{i}h_{j} \right\rangle_{data}$, mentre nel secondo quelli per il calcolo di $\left\langle v_{i}h_{j} \right\rangle_{recon}$.
Soffermiamoci per ora sul primo caso. Dalla \eqref{eq17} è possibile osservare che un nodo nascosto si comporta all'incirca come un \textit{percettrone} dello strato visibile.
\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{img/perceptron.png}
	\caption{Rappresentazione logica di un percettrone con 4 unità di input} \label{percettrone}
\end{figure}
\begin{definition}[{\cite{mitch}}] \label{def9} \definitions{Percettrone}
	Un \textit{percettrone} è un'unità di base di alcune \textit{Artificial Neural Networks}, il quale prende in input un vettore a valori reali $\textbf{x} \in \mathbb{R}^{n}$ con componenti $x_{1}, \dotsc, x_{n}$ e calcola una combinazione lineare degli stessi. Il percettrone avrà output $o(x_{1}, \dots, x_{n}) = 1$ se la combinazione lineare supera una certa \textit{soglia} $-w_{0}$, ovvero:
	$$
		o(x_{1}, \dots, x_{n}) = \bigg\lbrace
		\begin{tabular}{ll}
			1 & se $w_{0} + w_{1}x_{1} + \dots + w_{n}x_{n} > 0$, \\ 
			0 & altrimenti.
		\end{tabular} 
	$$
\end{definition}
Le differenze tra il modello classico di percettrone (definito nella \defref{def9} ed esemplificato in \figref{percettrone}) ed il nostro sono evidenti: il vettore \textbf{v} è a valori binari e non reali, inoltre lo stato di un nodo nascosto di una RBM dipende dalla combinazione lineare dei valori del vettore pesati per la forza della connessione $s_{i,j}$ attraverso una funzione di attivazione (la sigmoide $\sigma(x)$). L'ultima fondamentale differenza sta nel valore di soglia: le RBM sono reti neurali stocastiche, quindi il loro comportamento non è deterministico, bensì dipende da una funzione di probabilità; il valore del nodo nascosto $h_{j}$ segue una distribuzione Bernoulliana di parametro $p$ dato dalla \eqref{eq17}. In una rete con $|V|=n$, è possibile ridefinire lo stesso concetto nel seguente modo:
\begin{equation} \label{eq20}
	h_{j} = \bigg\lbrace 
		\begin{tabular}	{ll}
			1 & se $\sigma ( b_{j} + v_{1}w_{1j} + \dots + v_{n}w_{nj}) > r$, \\
			0 & altrimenti,
		\end{tabular}
\end{equation}
dove $r$ è una soglia randomica estratta in modo uniforme dall'intervallo $[0,1]$. Questo modello viene anche chiamato \textit{unità di soglia sigmoide} o \textit{unità ad attivazione sigmoide} (\textit{sigmoid threshold unit} \cite{mitch}), rappresentata in \figref{sigmoid} e in grado di applicare un concetto di sogliatura maggiormente differenziato in base al problema e di mappare un vasto dominio (tutti i vettori di dimensione $n$) in un codominio estremamente ristretto ($\{0,1\}$). Inoltre, la sigmoide gode di una regola derivativa molto semplice ($\frac{\partial \sigma (x)}{\partial x} = \sigma (x) (1- \sigma (x))$), caratteristica che la rende particolarmente adatta per l'utilizzo nei problemi di minimizzazione e quindi anche per l'apprendimento.
\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{img/sigmoid.png}
	\caption{Una \textit{sigmoid threshold unit} con 4 unità di input} \label{sigmoid}
\end{figure}

Per ottenere il valore di $\left\langle v_{i}h_{j} \right\rangle_{data}$ si ripete il calcolo della \eqref{eq20} per tutti i vettori di training $\textbf{v} \in S$ e si valuta la frequenza relativa degli $1$. Lo stesso procedimento deve essere ripetuto per tutti i $1 \leq j \leq |H|$ e poi nella direzione opposta per tutti i nodi visibili per ottenere $\left\langle v_{i}h_{j} \right\rangle_{recon}$.

Il processo di apprendimento può essere terminato usando unicamente l'informazione locale, senza necessità di memoria, quando $||\textbf{w}^{(i)} - \textbf{w}^{(i-1)}||_{\infty} < t$ per $K$ iterazioni consecutive dell'algoritmo, dove $t$ è un valore arbitrariamente piccolo di soglia, $K \in \mathbb{N}$, $\textbf{w}^{(i)}$ è il vettore dei pesi di dimensione $|V| \times |H|$ dei pesi associati agli archi del grafo nell'$i$-esima iterazione e $||\textbf{x}||_{\infty}$ è la norma infinito del vettore, che corrisponde alla componente di modulo massimo del vettore stesso. In questo modo ciò che viene valutata è la massima variazione nelle forze della rete in due iterazioni consecutive; se tale variazione è sufficientemente piccola, significa che tutte le connessioni si stanno stabilizzando attorno al valore di energia minima e che il processo di apprendimento sta giungendo a convergenza.

Possiamo infine formalizzare questo procedimento in nell'\aref{learn}, che costituisce una prima rappresentazione procedurale di programma eseguibile dal calcolatore per l'apprendimento di un dataset.

\input{chapter2/learning_algorithm.tex}

Avendo implementato le RBM tramite l'utilizzo di semplici matrici, l'\aref{learn} si presta a una serie di modifiche che permettono di snellirlo, rendendolo più leggibile e, grazie alle librerie scientifiche dedicate (vedi Paragrafo \ref{sssecpy}), anche meglio ottimizzato. In particolare, si possono generare i feature detector per tutti gli esempi di training contemporaneamente tramite un semplice prodotto righe per colonne tra la matrice degli esempi e quella dei pesi, per poi confrontare il risultato con una matrice della stessa dimensione a valori generati casualmente in $[0,1]$. Un ulteriore prodotto permetterà di ottenere $\left\langle v_{i}h_{j} \right\rangle_{data}$; mentre un procedimento analogo viene seguito per ottenere $\left\langle v_{i}h_{j} \right\rangle_{recon}$ a partire dallo stato dei nodi nascosti ottenuto al passaggio precedente. Tutti i dettagli implementativi più importanti verranno osservati nel dettaglio nel Capitolo \ref{sec3}.