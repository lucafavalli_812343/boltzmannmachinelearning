Le macchine di Boltzmann \cite{aarts} possono essere viste come reti costituite da un certo numero di unità, ciascuna delle quali può assumere due valori che chiameremo rispettivamente \textit{off} e \textit{on} o, per semplificare la modellazione matematica, 0 e 1.
\begin{definition}	\label{def:boltz}	\definitions{Macchina di Boltzmann}
	Una macchina di Boltzmann può essere definita come un grafo pesato $\mathcal{B} = (\mathcal{U},\mathcal{C})$, dove $\mathcal{U}$ è l'insieme dei nodi del grafo e $\mathcal{C}$ l'insieme delle connessioni tra i nodi. A sua volta, ciascun $c_{u,v} \in \mathcal{C}$ sarà definito da una tripletta $c_{u,v} = \{u, v, s_{u,v}\}$ dove $u,v \in \mathcal{U}$ e $s_{u,v} \in \mathbb{R}$, ovvero da una coppia di nodi e dalla forza della loro connessione. 
\end{definition}
In una macchina di Boltzmann valgono le seguenti relazioni:
\begin{equation} \label{eq2}
	c_{u,v} \in \mathcal{C} \implies c_{v,u} \in \mathcal{C}\,,
\end{equation}
\begin{equation} \label{eq3}
	s_{u,v} = s_{v,u}\,.
\end{equation}
\begin{definition} \label{def4} \definitions{Vicinato}
	Si dice \textit{vicinato} di $u \in \mathcal{U}$ l'insieme $\mathcal{N}_{u} = \{ v \;|\; c_{u,v} \in \mathcal{C}\}\,.$
\end{definition}

In generale ci occuperemo delle macchine di Boltzmann ristrette \cite{hint2, hint, hint3}, reti $\mathcal{B} = (\mathcal{U},\mathcal{C})$ per le quali vengono definiti i sottoinsiemi (strati) $V\subseteq \mathcal{U}$ e $H\subseteq \mathcal{U}$, chiamati rispettivamente strato (o livello) visibile e strato nascosto tali che:
\begin{itemize}
	\item $V \cup H = \mathcal{U}$;
	\item $V \cap H = \emptyset$;
	\item $\forall v \in V,\; \forall h \in H,\;\exists c_{v,h} \in \mathcal{C}$ che connette $v$ ed $h$;
	\item $\forall v, v' \in V$ $\nexists c_{v,v'} \in \mathcal{C}$ che connette $v$ e $v'$;
	\item $\forall h, h' \in H$ $\nexists c_{h,h'} \in \mathcal{C}$ che connette $h$ e $h'$.
\end{itemize}
Le ultime tre condizioni possono essere riassunte nel modo seguente:
\begin{equation} \label{eq4}
\forall v \in V, \: \forall h \in H, \: \mathcal{N}_{v} \equiv H, \: \mathcal{N}_{h} \equiv V \,.
\end{equation}
\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{img/rbm.png}
	\caption{Schema delle connessioni di una RBM con tre unità visibili e due unità nascoste} \label{rbm:nodes}
\end{figure}
Riassumendo, $V$ ed $H$ formano una partizione di $\mathcal{U}$, inoltre tutti i nodi dello strato visibile sono connessi con tutti e soli i nodi dello strato nascosto e viceversa. \uppercase{è} possibile osservare un'istanza di questo modello in \figref{rbm:nodes}.

Un lato mancante del grafo può essere anche visto come una connessione per la quale vale $s_{u,v}=0$; questo punto di vista torna particolarmente utile quando si vuole supervisionare l'apprendimento della rete: è possibile inibire alcune connessioni semplicemente forzandone il valore di $s$ a 0 tramite una maschera. Nel Paragrafo \ref{ssseclearn} saranno illustrati nel dettaglio i meccanismi tramite i quali le reti neurali sono in grado di evolvere la propria rappresentazione interna e il modo in cui gli input vengono rielaborati dalla rete per produrre una risposta. 

Per tutte le considerazioni che seguono, supporremo di aver stabilito un ordinamento totale sull'insieme $\mathcal{U}$. In ogni istante, una macchina di Boltzmann è descritta dalla sua \textit{configurazione}.
\begin{definition}[{\cite{aarts}}] \label{def2} \definitions{Configurazione}
	La \textit{configurazione k} di una RBM è univocamente definita da una sequenza di dimensione $|\mathcal{U}|$, in cui l'$u$-esima componente $k(u) \in \{0,1\}$ denota lo \textit{stato} dell'unità $u$ nella configurazione $k$.
	Lo \textit{spazio delle configurazioni $\mathcal{R}$} è l'insieme di cardinalità $2^{|\mathcal{U}|}$ di tutte le possibili configurazioni.
\end{definition}
\begin{definition}[{\cite{aarts}}] \label{def3} \definitions{Attivazione}
	Una connessione $c_{u,v} \in \mathcal{C}$ si dice \textit{attivata} nella configurazione $k \in \mathcal{R}$ se $k(u)k(v)=1$.
\end{definition}

In una RBM con strato visibile $V$ e strato nascosto $H$, per ciascuna configurazione $k$, definiamo i vettori \textbf{v} $\in \{1,0\}^{|V|}$ e \textbf{h} $\in \{1,0\}^{|H|}$ generati rispettivamente dallo stato dei nodi visibili e da quello dei nodi nascosti. \uppercase{è} possibile definire il valore di energia associato a \textbf{v} e \textbf{h} come:
\begin{equation} \label{eq5}
	E(\textbf{v},\textbf{h}) = - \sum\limits_{v \in \textbf{v}} b_{v}k(v) - \sum\limits_{h \in \textbf{h}}b_{h}k(h) - \sum\limits_{v \in \textbf{v}, h \in \textbf{h}}k(v)k(h)s_{v,h}\;,
\end{equation}
dove $k$, $k(v)$ e $k(h)$ sono definite come nella \defref{def2} e $b_{u}$ è l'eventuale soglia (vedi la nota \ref{notebias} di questo capitolo) associata a un nodo $u \in \mathcal{U}$ \cite{hint3} ed è definita come $s_{u,u}$; in alternativa è possibile predisporre un nodo di soglia sempre \textit{on} al quale sono collegati tutti gli altri nodi. Chiaramente, solo le connessioni attivate in $k$ contribuiscono al calcolo dell'energia. A ciascuna possibile coppia di vettori \textbf{v} e \textbf{h} è assegnato un valore di probabilità dato da:
\begin{equation} \label{eq6}
	p(\textbf{v},\textbf{h}) = \frac{1}{Z} \, \mathrm{e}^{-E(\textbf{v},\textbf{h})}\;,
\end{equation}
dove
\begin{equation} \label{eq7}
	 Z = \sum\limits_{\textbf{v'}, \textbf{h'}}\mathrm{e}^{-E(\textbf{v'},\textbf{h'})}\;
\end{equation}
e \textbf{v'},\textbf{h'} indicano tutti i possibili vettori generati dallo stato dei nodi degli insiemi visibile e nascosto, ovvero di tutti i possibili elementi di $\{1,0\}^{|V|}$ e $\{1,0\}^{|H|}$. Dalla \eqref{eq6} appare evidente che è possibile aumentare la probabilità associata a una coppia di vettori $(\textbf{v},\textbf{h})$ riducendo il valore della loro energia e aumentando quello delle altre possibili coppie, processo che avviene tramite la modifica dei pesi $s_{v,h}$. \uppercase{è} proprio in questo processo che consiste l'apprendimento e il vettore \textbf{v} viene anche definito \textit{immagine di training} della RBM. L'insieme di tutte le immagini di training prende il nome di \textit{training set S}. Dalla \eqref{eq6} otteniamo la probabilità associata a un'immagine di training:
\begin{equation} \label{eq8}
	p(\textbf{v}) = \frac{1}{Z} \, \sum\limits_{\textbf{h'}} \mathrm{e}^{-E(\textbf{v},\textbf{h'})}\;.
\end{equation}
Durante l'apprendimento, la RBM aggiusta i propri pesi per aumentare la probabilità di ciascuno dei vettori $\textbf{v} \in S$, fino a convergenza, ovvero fino a trovare un minimo globale (o una sua approssimazione, come discusso nel Capitolo \ref{sec1}) per la \eqref{eq5}. Questo modello mantiene la nostra analogia con il cervello: i neuroni sono cellule costituite da un corpo cellulare (soma) e da un insieme di estroflessioni citoplasmatiche di due tipi chiamate dendriti e assone. Attraverso i dendriti (le estroflessioni più corte e presenti in maggior quantità) i neuroni ricevono l'input proveniente dalle altre cellule neuronali della rete. Se il potenziale d'azione che raggiunge il corpo cellulare supera una certa soglia, l'output viene trasmesso lungo l'assone (la protuberanza più lunga, unica per ciascuna cellula) e attraverso le sinapsi, spazi che dividono due neuroni consecutivi (detti rispettivamente presinaptico e postsinaptico). All'interno di ciascun assone lo stimolo elettrico provoca il rilascio di specifiche sostanze chiamate neurotrasmettitori da parte delle proteine di membrana del neurone presinaptico e all'interno della sinapsi; queste sostanze interagiscono con i recettori presenti sui dendriti del neurone postsinaptico. Questo processo bioelettrico permette il passaggio di informazioni tra neuroni. I nodi delle reti neurali sono costruiti per emulare questa struttura (vedi il paragrafo successivo); inoltre, proprio come nelle RBM, i neurotrasmettitori possono essere eccitativi o inibitori. La differenza tra questi due tipi di stimoli determina lo stato interno del neurone, analogamente a quanto avviene nella \eqref{eq1}, e contribuisce a intensificare o rendere più deboli le connessioni sinaptiche.

Nell'ambito dell'apprendimento e della classificazione, è utile il concetto di \textit{funzione consenso}, una funzione il cui valore indica il consenso raggiunto dalle unità che costituiscono la macchina di Boltzmann relativamente allo stato di ciascuna di esse. L'obiettivo è di raggiungere una \textit{configurazione massima}, ovvero una configurazione in cui la funzione consenso ha valore massimo.
\begin{definition} [{\cite{aarts}}] \label{def5} \definitions{Funzione consenso}
	La \textit{funzione consenso} C: $\mathcal{R}\to\mathbb{R}$ associa a una configurazione $k \in \mathcal{R}$ la somma dei pesi delle connessioni attivate in $k$:
	\begin{equation} \label{eq9}
		\mbox{C}(k) = \sum\limits_{c_{u,v}\in \mathcal{C}} k(u)k(v)s_{u,v}\;.
	\end{equation}
\end{definition}
Ad esempio, per classificare un dato, si sottopone a un sottoinsieme delle unità della rete già addestrata (lo strato visibile) la rappresentazione binaria del dato stesso, per poi far evolvere la rete; i rimanenti nodi liberi (lo strato nascosto) modificheranno il proprio stato fino al raggiungimento del massimo consenso: i valori finali delle unità libere costituiranno la classificazione desiderata. Una funzione consenso è dotata di tanti massimi locali quante sono le possibili classificazioni (output) che è possibile ottenere; ciò significa che, a patto di possedere un numero sufficiente di nodi e di essere stata addestrata fino a convergenza (vedi il paragrafo seguente), una macchina di Boltzmann è sempre in grado di riconoscere un dato del campione e classificarlo in modo univoco, ma possiede anche capacità associative, ovvero è in grado di assegnare una classificazione anche a dati mai osservati prima, semplicemente tramite il riconoscimento delle similitudini (feature in comune) che il nuovo dato ha con quelli che la macchina è in grado di classificare in modo deterministico. \uppercase{è} proprio grazie alle proprie capacità associative che le macchine di Boltzmann superano un classificatore deterministico in molti problemi reali, come ad esempio il riconoscimento di caratteri manoscritti \cite{hint}. Alcuni modelli necessitano di una struttura a più strati per raggiungere lo stesso risultato (vedi Paragrafo \ref{sssecrbmmult}).